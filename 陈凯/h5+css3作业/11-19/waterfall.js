window.onload = function () {
    document.body.onresize = function () {
        let win = document.body.offsetWidth;
        let scroll = window.screen.width;
        let header = document.getElementById("header");
        if(win > scroll * 0.95){
            header.style.columnCount = 5;
        }else if(win < scroll * 0.95 && win > scroll * 0.76){
            header.style.columnCount = 4;
        }else if(win < scroll * 0.76 && win > scroll * 0.62){
            header.style.columnCount = 3;
        }else if(win < scroll * 0.62 && win > scroll * 0.38){
            header.style.columnCount = 2;
        }else if (win < scroll * 0.38){
            header.style.columnCount = 1;
        }
    }
}
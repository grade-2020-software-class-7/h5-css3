$(function(){
    //背景颜色
    let colors = $('span[color]');
    for(let i = 0; i < colors.length ;i++){
        let color = colors[i];
        let n = $(color).attr('color');
        //设置span标签的颜色
        $(color).css('background-color',n);
        //点击更改背景色
        $(color).click(function(){
            $('body').css('background',n);

        })
    }

    //字体大小
    let sizes = $('u[size]');
    for(let i = 0; i < sizes.length ;i++){
        let size = sizes[i];
        let n = $(size).attr('size');
        //点击更改字体大小
        $(size).click(function(){
            $('p').css('font-size',n);
        })
    }

    //手机版点击出现导航
    $('body').click(function(){
        if($('body').width()<500){
            $('.black').slideToggle(300);
        }
    })
})
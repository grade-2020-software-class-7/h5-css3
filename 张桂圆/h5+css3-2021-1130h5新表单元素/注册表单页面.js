window.onload = function () {
    let input = document.querySelectorAll('input')
    let submit = input[10]
    let spanerror = document.querySelectorAll('.span-error')
    let vipname = input[0];
    let password = input[1];
    let repassword = input[2]
    let username = input[3]
    let compalyname = input[4];
    let phone = input[8]
    // submit.addEventListener('change', function () {
    //     for (let j = 0; j < input.length; j++) {
    //         if (input[j] == '') {
    //             input[j].style.border = '1px solid red'
    //         }
    //     }
    // })
    submit.addEventListener('click', function () {
        for (let i = 0; i < input.length; i++) {
            if (i == 0) {
                vipnamecheck()
                input[i].style.border = '1px solid red'
            }
            if (i == 1) {
                passwordcheck();
                input[i].style.border = '1px solid red'
            }
            if (i == 2) {
                repasswordcheck()
                input[i].style.border = '1px solid red'
            }
            if (i == 3) {
                usernamecheck()
                input[i].style.border = '1px solid red'
            }
            if (i == 4) {
                compalynamecheck();
                input[i].style.border = '1px solid red'
            }
            if (i == 8) {
                phonecheck()
                input[i].style.border = '1px solid red'
            }
        }
    })
    
    function vipnamecheck() {
        let reg = /^(?!(\d+)$)[\u4e00-\u9fffa-zA-Z\d\-_]+$/;
        let errors = spanerror[0]
        if (vipname.value == '') {
            errors.style.display = 'block'
            errors.textContent = '请设置会员名';
            errors.style.color = 'red'
            // vipname.style.borderColor = 'red'
        } else {
            if (reg.test(vipname.value)) {
                errors.style.display = 'none'
                vipname.style.border = '1px solid black'
            } else {
                errors.style.display = 'block'
                // errors.textContent = '5-25字符，可以包含字母或者汉字';
                errors.style.color = 'red'
                // vipname.style.borderColor = 'red'
            }
            vipname.style.border = '1px solid black'
        }
    }
    function passwordcheck() {
        let reg = /^(?![A-Za-z]+$)(?!\\d+$)(?![\\W_]+$)\\S{6,20}$/;
        let errors1 = spanerror[1]
        if (password.value == '') {
            errors1.style.display = 'block'
            errors1.textContent = '请设置登录密码';
            errors1.style.color = 'red'
        } else {
            if (reg.test(password.value)) {
                errors1.style.display = 'none'
                // password.style.border='1px solid black'
            } else {
                errors1.style.display = 'block'
                errors1.style.color = 'red'
            }
        }
        password.style.border = '1px solid black'
    }
    function repasswordcheck() {
        let errors2 = spanerror[2]
        if (repassword.value == '') {
            errors2.style.display = 'block'
            errors2.textContent = '请再次确认登录密码';
            errors2.style.color = 'red'
        } else {
            if (repassword == password) {
                errors2.style.display = 'none'
            } else {
                errors2.style.display = 'block'
                // errors2.textContent = '两次输入的密码不一致';
                errors2.style.color = 'red'
            }
        }
        repassword.style.border = '1px solid black'
    }
    function usernamecheck() {
        let errors3 = spanerror[3]
        let reg3 = /^([\u4e00-\u9fa5]{1,20}|[a-zA-Z\.\s]{2,20})$/;
        if (username.value == '') {
            errors3.style.display = 'block'
            errors3.textContent = '请输入真实姓名';
            errors3.style.color = 'red'
        } else {
            if (reg3.test(username.value)) {
                errors3.style.display = 'none'
            } else {
                errors3.style.display = 'block'
                // errors3.textContent = '不能全为数字,不能带有特殊标点符号';
                errors3.style.color = 'red'
            }
        }
        username.style.border = '1px solid black'
    }
    function compalynamecheck() {
        let errors4 = spanerror[4]
        let reg3 = /^([\u4e00-\u9fa5]{1,20}|[a-zA-Z\.\s]{2,20})$/;
        if (compalyname.value == '') {
            errors4.style.display = 'block'
            errors4.textContent = '请输入真实姓名';
            errors4.style.color = 'red'
            // compalyname.style.outlineColor = 'red'
            // compalyname.style.borderColor = 'red'
        } else {
            if (reg3.test(compalyname.value)) {
                errors4.style.display = 'none'
            } else {
                errors4.style.display = 'block'
                // errors4.textContent = '不能全为数字,不能带有特殊标点符号';
                errors4.style.color = 'red'
                // compalyname.style.outlineColor = 'red'
                // compalyname.style.borderColor = 'red'
            }
        }
        compalyname.style.border = '1px solid black'
    }
    function phonecheck() {
        let errors5 = spanerror[5]
        let reg4 = /^1(3[0-9]|5[189]|8[6789])[0-9]{8}$/;
        if (phone.value == '') {
            errors5.style.display = 'block'
            errors5.textContent = '请输入手机号';
            errors5.style.color = 'red'
            // compalyname.style.outlineColor = 'red'
            // compalyname.style.borderColor = 'red'
        } else {
            if (reg4.test(phone.value)) {
                errors5.style.display = 'none'
            } else {
                errors5.style.display = 'block'
                errors5.style.color = 'red'
                // compalyname.style.outlineColor = 'red'
                // compalyname.style.borderColor = 'red'
            }
        }
        phone.style.border = '1px solid black'
    }

}
window.onload = function () {

    let button = document.querySelector("button")
    let inputs = document.querySelectorAll(".item4")
    console.log(inputs);

    let inputtip = document.querySelectorAll(".inputtip")
    // 点击同意并注册按钮，当输入框为空时，输入框变为红色
    button.addEventListener('click', function () {
        inputs.forEach(function (n, i) {
            console.log(n);
            if (n.value == "") {
                console.log(n);
                n.style.border = "1px solid red"
                n.parentElement.nextElementSibling.style.display = "block"
            }
            if (i == 3) {
                n.previousElementSibling.style.border = "1px solid red"
            }
        })
    })

    // 会员名
    let reg = /^\d*$/;
    let reg2 = /[a-zA-Z0-9\u4E00-\u9FFF]{5,25}/
    let patrn = /[`~!@#$%^&*()_\-+=<>?:"{}|,.\/;'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】、；‘’，。、]/;

    inputs[0].onfocus = function () {
        reminBox.style.display = "block"
    }

    inputs[0].onblur = function () {
        reminBox.style.display = "none"
        if (reg.test(inputs[0].value) || !(reg2.test(inputs[0].value))) {
            inputs[0].style.border = "1px solid red"
            inputtip[0].innerHTML = "5-25个字符，需要包含字母或汉字"
            inputtip[0].style.display = "block"
        }
        if (patrn.test(inputs[0].value)) {
            inputs[0].style.border = "1px solid red"
            inputtip[0].innerHTML = "该会员名包含了非法字符，请重新输入"
            inputtip[0].style.display = "block"
        }
        if (!(reg.test(inputs[0].value)) && reg2.test(inputs[0].value) && !(patrn.test(inputs[0].value))) {
            inputs[0].style.border = ""
            inputtip[0].style.display = "none"
        }
        if (inputs[0].value == "") {
            inputs[0].style.border = ""
            inputtip[0].style.display = "none"
        }
    }

    

    //登录密码
    let pwreg2 = /(?!^[0-9]+$)(?!^[A-z]+$)(?!^[^A-z0-9]+$)^.{6,20}$/;
    inputs[1].onblur = function () {
        if (!(pwreg2.test(inputs[1].value))) {
            inputs[1].style.border = "1px solid red"
            inputtip[1].innerHTML = "密码设置不符合要求"
            inputtip[1].style.display = "block"
        }
        if (inputs[1].value == inputs[0].value) {
            inputs[1].style.border = "1px solid red"
            inputtip[1].innerHTML = "密码和会员名太过相似"
            inputtip[1].style.display = "block"
        }
        if (pwreg2.test(inputs[1].value) && inputs[1].value != inputs[0].value) {
            inputs[1].style.border = ""
            inputtip[1].style.display = "none"
        }
        if (inputs[1].value == "") {
            inputs[1].style.border = ""
            inputtip[1].style.display = "none"
        }
    }

    //密码确认
    inputs[2].onblur = function () {
        if (inputs[1].value != inputs[2].value) {
            inputs[2].style.border = "1px solid red"
            inputtip[2].innerHTML = "两次输入的密码不一致，请重新输入"
            inputtip[2].style.display = "block"
        } if (inputs[1].value == inputs[2].value) {
            inputs[2].style.border = ""
            inputtip[2].style.display = "none"
        }
        if (inputs[2].value == "") {
            inputs[2].style.border = ""
            inputtip[2].style.display = "none"
        }
    }

    // 手机号码
    let phoneReg = /^1\d{10}$/
    inputs[3].onblur = function () {
        if (!(phoneReg.test(inputs[3].value))) {
            inputs[3].style.border = "1px solid red"
            inputtip[3].innerHTML = "手机号格式不正确，请重新输入"
            inputtip[3].style.display = "block"
        } if (phoneReg.test.inputs[3].value) {
            inputs[3].style.border = ""
            inputtip[3].style.display = "none"
        }
        if (inputs[3].value == "") {
            inputs[3].style.border = ""
            inputtip[3].style.display = "none"
        }
    }



}
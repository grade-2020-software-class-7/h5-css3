# Canvas

## 课前回顾

上节课，我们学习了H5的新的语义标签，和多媒体标签。同学们需要慢慢更改自己写HTML代码的习惯，尽量写出具有合理语义结构的代码。

## 本节目标

- 了解Canvas是什么
- 能够在画布上绘制基本图形
- 能够实现简单的动画效果

## 1. 概述

`<canvas>`元素用语生成图像。它本身就像一个画布， JavaScript 通过操作它的 API，在上面生成图像。它的底层是一个个像素，基本上`<canvas>`是一个可以用 JavaScript 操作的位图（bitmap）。 

使用 Canvas API 之前，需要在网页里面新建一个`<canvas>`元素。 

```html
<canvas id="myCanvas" width="400" height="250">
  您的浏览器不支持 Canvas
</canvas>
```

如果浏览器不支持这个 API，就会显示`<canvas>`标签中间的文字：“您的浏览器不支持 Canvas”。 

每个`<canvas>`元素都有一个对应的`CanvasRenderingContext2D`对象。Canvas API 就定义在这个对象上面。 

```js
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');
```

> 我们会发现，canvas相关属性和方法在VSCode里面并没有给我们提示。这样不方便我们学习。
>
> **解决方法**
>
> ```js
> /** @type {HTMLCanvasElement} */
> // 一定要放在下面这个语句前面
> var canvas = document.getElementById('myCanvas');
> ```

上面代码中，`<canvas>`元素节点对象的`getContext()`方法，返回的就是`CanvasRenderingContext2D`对象。 

注意，Canvas API 需要`getContext`方法指定参数`2d`，表示该`<canvas>`节点生成 2D 的平面图像。如果参数是`webgl`，就表示用于生成 3D 的立体图案，这部分属于 WebGL API。 

## 2. 绘制图形

Canvas 画布提供了一个作图的平面空间，该空间的每个点都有自己的坐标。原点`(0, 0)`位于图像左上角，`x`轴的正向是原点向右，`y`轴的正向是原点向下。 

### 2.1 路径

以下方法和属性用来绘制路径。

```js
let canvas = document.getElementById('myCanvas');
let ctx = canvas.getContext('2d');
```

- `ctx.beginPath()`：开始绘制路径。
- `ctx.closePath()`：关闭路径，它会试图从当前路径的终点连一条路径到起点，让整个路径闭合起来。如果图形已经封闭，或者只有一个点，那么此方法不会产生任何效果。
- `ctx.moveTo(x, y)`：设置路径的起点，即将一个新路径的起始点移动到`(x, y)`坐标。
- `ctx.lineTo(x, y)`：使用直线从当前点连接到`(x, y)`坐标。
- `ctx.fill()`：在路径内部填充颜色（默认为黑色）。
- `ctx.stroke()`：路径线条着色（默认为黑色）。
- `ctx.fillStyle`：指定路径填充的颜色和样式（默认为黑色）。
- `ctx.strokeStyle`：指定路径线条的颜色和样式（默认为黑色）。

```js
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');
console.log(ctx);
// 开始绘制路径
ctx.beginPath();
// 将初始位置设定到（100， 100）位置
ctx.moveTo(100, 100);
// 连线到（200， 200）的位置
ctx.lineTo(200, 200);
// 再连线到（100，200）的位置
ctx.lineTo(100, 200);
```

上面代码只是确定了路径的形状，画布上还看不出来，因为没有颜色。所以还需要着色。 

```js
ctx.fill();
// 或者
ctx.stroke();
```

上面代码中，这两个方法都可以使得路径可见。`fill()`在路径内部填充颜色，使之变成一个实心的图形；`stroke()`只对路径线条着色。 

可以使用`fillStyle`和`stokeStyle`属性指定其他颜色。

```js
ctx.fillStyle = 'red'; // 属性设置需要放在方法执行前面，不然无法生效。
ctx.fill();

// 或者
ctx.strokeStyle = 'red';
ctx.stroke();
```

### 2.2 线型

以下的方法和属性控制线条的视觉特征。

- `ctx.lineWidth`：指定线条的宽度，默认为`1.0`。
- `ctx.lineCap`：指定线条末端的样式，有三个可能的值：
  - `butt`：默认值，末端为矩形。
  - `round`：末端为圆形。
  - `square`：末端为突出的矩形，矩形宽度不变，高度为线条宽度的一半。
- `ctx.lineJoin`：指定线段交点的样式，有三个可能的值：
  - `round`：交点为扇形。
  - `bevel`：交点为三角形底边。
  - `miter`：默认值，交点为菱形。
- `ctx.miterLimit`：指定交点菱形的长度，默认为`10`。该属性只在`lineJoin`属性的值等于`miter`时有效。
- `ctx.getLineDash()`：返回一个数组，表示虚线里面线段和间距的长度。
- `ctx.setLineDash()`：数组，用于指定虚线里面线段和间距的长度。

```js
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');

ctx.beginPath();
ctx.moveTo(100, 100);
ctx.lineTo(200, 200);
ctx.lineTo(100, 200);

ctx.lineWidth = 3;
ctx.lineCap = 'round'; // 线条末端设置为圆角
ctx.lineJoin = 'round'; // 线条交接点设置为圆角
ctx.setLineDash([15, 5]); // 设置为虚线
ctx.stroke();
```

### 2.3 矩形

以下方法用来绘制矩形。

- `ctx.rect()`：绘制矩形路径。
- `ctx.fillRect()`：填充一个矩形。
- `ctx.strokeRect()`：绘制矩形边框。
- `ctx.clearRect()`：指定矩形区域的像素都变成透明。

上面四个方法个格式都一样，都接受四个参数，分别是矩形左上角的横坐标和纵坐标，矩形的宽和高。

```js
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');

ctx.rect(10, 10, 100, 100); // 绘制路径
ctx.fill(); // 着色，实心矩形
// 空心矩形
ctx.rect(110, 10, 100, 100);
ctx.stroke(); // 线条着色，空心矩形
```

`fillRect()`用来向一个矩形区域填充颜色：

```js
ctx.fillStyle = 'green';
ctx.fillRect(110, 110, 100, 100); // 一步到位，绘制路径并填色了
```

`strokeRect()`用来绘制一个矩形区域的边框。

```js
ctx.strokeStyle = 'blue';
ctx.strokeRect(110, 10, 100, 100); // 一步到位，绘制路径并填色了
```

`clearRect()`用于擦除指定矩形区域的像素颜色，等同于把早先的绘制效果都去除。

```js
ctx.fillRect(10, 10, 100, 100); // 绘制一个实心矩形
ctx.clearRect(15, 15, 90, 90); // 擦除一个矩形
```

我们可以看到`clearRect()`会将参数设定的区域的内容擦除。

### 2.4 弧线

以下方法用于绘制弧形。

- `ctx.arc()`：通过指定圆心和半径绘制弧形。
- `ctx.arcTo()`：通过制定两根切线和半径绘制弧形。

**`arc()`语法：**

```
ctx.arc(x, y, radius, startAngle, endAngle, anticlockwise);
```

![](images/arc.gif)

- `x, y` ：是圆心的坐标
- `radius`：是半径
- `startAngle` 和`endAngle`则是扇形的起始角度和终止角度（以弧度表示）
- `anticlockwise`：表示做图时，应该逆时针画（true）还是顺时针画（false），这个参数用来控制扇形的方法（比如上半圆还是下半圆）

绘制一个实心圆：半径为50， 起始角度为0， 终止角角度为2 * PI的完整的圆。

```js
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');
// 开始绘制
ctx.beginPath();  // 绘制弧线，需要调用beginPath()方法
ctx.arc(60, 60, 50, 0, Math.PI*2, true);
ctx.fill();
```

绘制一个空心圆：

```js
ctx.beginPath();
ctx.arc(100, 20, 50, 0, Math.PI * 2, false);
ctx.stroke();
```

利用`closePath()`方法，可以绘制一个缺了一块的圆。

```js
ctx.beginPath();
ctx.arc(100, 20, 50, 0, Math.PI * 1.7, false);
ctx.closePath(); // 关闭绘制，会将绘制终点和起点用线连接起来。
ctx.stroke();
```

**`arcTo()`**用来绘制圆弧，需要给出两个点的坐标，当前点与第一个点形成一条直线，第一个点与第二个点形成另一条直线，然后画出与这两根直线相切的弧线。

**语法**

```
ctx.arcTo(x1, y1, x2, y2, radius);
```

- (x1, y1)：第一个点的坐标
- (x2, y2)：第二个点的坐标
- radius：半径

```js
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');

ctx.beginPath();
ctx.moveTo(20, 20); // 创建开始点
ctx.arcTo(100, 20, 100, 100, 25); // 根据给出的两个点坐标，绘制弧线
ctx.lineTo(100, 100); // 弧线绘制完成后，当前点在弧线与第二条线的切点上。再画一条线，将这两点连起来。
ctx.stroke();
```

### 2.5 文本

以下方法和属性用于绘制文本。

- `ctx.fillText()`：在指定位置绘制实心字符。
- `ctx.strokeText()`：在指定位置绘制空心字符。
- `ctx.font`：指定字型大小和字体，默认值为`10px sans-serif`。
- `ctx.textAlign`：文本的对齐方式，默认值为`start`。

#### 2.5.1 `fillText()`

`fillText()`用来绘制实心字符。

```
ctx.fillText(text, x, y [, maxWidth])
```

该方法接受4个参数：

- `text`：所要填充的字符串。
- `x`,`y`：文字起点的坐标，单位像素。
- `maxWidth`：文本的最大像素宽度。该参数可选，如果省略，则表示宽度没有限制。如果文本实际长度超过这个参数设置的值，那么浏览器将尝试用较小的字体填充。

```js
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');
ctx.fillText('Hello world', 50, 50);
```

上面代码在`(50, 50)`位置写入字符串`Hello world`。

**注意：**`fillText()`方法不支持文本断行，所有文本一定出现在一行内。如果要生成多行文本，只能调用多次`fillText()`方法。

#### 2.5.2 `strokeText()`

`strokeText()`方法用来添加空心字符，它的参数与`fillText()`一致。

```js
ctx.strokeText('Hello world', 50, 50);
```

#### 2.5.3 `font`

上面两种方法绘制的文本，默认都是`10px`大小、`sans-serif`字体。`font`属性可以改变字体设置。该属性的值是一个字符串，使用CSS的`font`属性即可。

```js
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');

ctx.font = 'Bold 20px Arial';
ctx.fillText('Hello world', 50, 50);
```

#### 2.5.4 `textAlign`

`textAlign`属性用来指定文本的对齐方式。它可以取以下几个值。

- `left`：左对齐
- `right`：右对齐
- `center`：居中
- `start`：默认值，起点对齐（从左到右的文本为左对齐，从右到左的文本为右对齐）
- `end`：结尾对齐（从左到右的文本为右对齐，从右到左的文本为左对齐）

```js
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');

ctx.font = 'Bold 20px Arial';
ctx.textAlign = 'center';
ctx.fillText('Hello world', 50, 50);
```

### 2.6 渐变色和图像填充

以下方法用于设置渐变色效果和图像填充效果。

- `ctx.createLinearGradient()`：定义线性渐变样式。
- `ctx.createRadialGradient()`：定义辐射渐变样式。

#### 2.6.1 `createLinearGradient()`

`createLinearGradient()`方法按照给定直线，生成线性渐变的样式。

```
ctx.createLinearGradient(x0, y0, x1, y1);
```

- `x0, y0`：设定起点的坐标
- `x1, y1`：设定终点的坐标

通过不同的坐标值，可以生成从上至下，从左到右的渐变等等。

该方法的返回值是一个`CanvasGradient`对象，该对象只有一个`addColorStop()`方向，用来指定渐变点的颜色。`addColorStop()`方法接受两个参数，第一个参数是0到1之间的一个位置量，0表示起点，1表示终点，第二个参数是一个字符串，表示CSS颜色。

```js
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');

var gradient = ctx.createLinearGradient(0, 0, 200, 0);
gradient.addColorStop(0, 'green');
gradient.addColorStop(1, 'white');
ctx.fillStyle = gradient; // 将渐变样式赋值给fillStyle属性
ctx.fillRect(10, 10, 200, 100); // 填充矩形区域
```

#### 2.6.2 `createRadialGradient()`

此方法定义一个辐射渐变，需要指定两个圆。

```
ctx.createRadialGradient(x0, y0, r0, x1, y1, r1)
```

- `x0, y0, r0`：辐射起始的圆的圆心坐标和半径
- `x1, y1, r1`：辐射终止的圆的圆心坐标和半径

该方法的返回值也是一个`CanvasGradient`对象。

```js
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');

var gradient = ctx.createRadialGradient(100, 100, 50, 100, 100, 100);
gradient.addColorStop(0, 'white');
gradient.addColorStop(1, 'green');
ctx.fillStyle = gradient;
ctx.fillRect(0, 0, 200, 200);
```

## 3. 动画效果

通过改变坐标，很容易在画布 Canvas 元素上产生动画效果。

```js
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');

var posX = 20;
var posY = 100;

setInterval(function () {
  ctx.fillStyle = 'black';
  ctx.fillRect(0, 0, canvas.width, canvas.height); // 将画布背景色设置为黑色

  posX += 1;
  posY += 0.25;

  ctx.beginPath();
  ctx.fillStyle = 'white';

  ctx.arc(posX, posY, 10, 0, Math.PI * 2, true);
  ctx.closePath();
  ctx.fill();
}, 30);
```

上面代码会产生一个小圆点，每隔30毫秒就向右下方移动的效果。

`setInterval()`函数的一开始，将画布重新渲染黑色底色，是为了抹去是上一步的小圆点。

上面例子通过设置圆心坐标，可以产生各种运动轨迹。下面是先上升后下降的例子。

```js
var vx = 10;
var vy = -10;
var gravity = 1;
setInterval(function() {
    postX += vx;
    postY += vy;
    vy += gravity;
    // ...
}, 30)
```

## 练习

同学课下自己练习雪花飘落的效果。这个效果网上可以看到很多参考答案，希望大家参考别人的代码，自己来实现这个效果。

> 实践出真章，只有多练，才能能好的理解canvas的方法和属性的作用和效果。一开始学习模仿别人是很正常的事情，通过别人的代码，理解别人的设计思路。这就是经验。

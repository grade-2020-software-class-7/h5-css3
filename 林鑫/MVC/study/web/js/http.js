function ajax({ url, method, data, success, error }) {
    const xhr = new XMLHttpRequest();

    // 事件监听
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                // 我们需要对不同的业务逻辑返回状态做处理
                let res = JSON.parse(xhr.responseText); // {code: '200', msg: '', data: undefined}
                if (res.code === '200') {
                    success(res.data);
                } else {
                    error(res);
                }
            } else {
                // 展示错误信息
                console.log(xhr.statusText);
                alert(xhr.statusText);
            }
        }
    };

    xhr.open(method, url);

    // 设置请求头的信息，必须放在open方法调用后
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    xhr.send(data || null);
}

function getQuery() {
    let s1 = location.search.substr(1);
    let arr = s1.split('=');
    let obj = {};
    for(let i = 0; i < arr.length; i+=2) {
        obj[arr[i]] = arr[i + 1];
    }

    return obj;
}

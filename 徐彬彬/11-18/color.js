window.onload = function(){
    let ranges = document.getElementsByClassName("range");
    for(let i = 0;i < ranges.length;i++){
        ranges[i].addEventListener("input",function(event){
            let name = event.target.name;
            if(name == "red"){
                let Value = event.target.value;
                let rgb = document.getElementById("R");
                rgb.innerHTML = Value;
                let bgColor = document.getElementById("rgb");
                bgColor.style.backgroundColor = ""+ bgColor.innerText +"";
            }else if(name == "green"){
                let Value = event.target.value;
                let rgb = document.getElementById("G");
                rgb.innerHTML = Value;
                let bgColor = document.getElementById("rgb");
                bgColor.style.backgroundColor = ""+ bgColor.innerText +"";
            }else if(name == "blue"){
                let Value = event.target.value;
                let rgb = document.getElementById("B");
                rgb.innerHTML = Value;
                let bgColor = document.getElementById("rgb");
                bgColor.style.backgroundColor = ""+ bgColor.innerText +"";
            }
        });
    }
}
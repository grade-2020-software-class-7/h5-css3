# 移动端touch,click事件的理解

移动端在touch上一共有4个事件：` touchstart`， `touchmove`， `touchend`， `touchcancel`。一般来说，他们的执行顺序是：` touchstart` ->` touchmove` ->` touchend` -> `touchcancel `. 其中`touchcancel`一般情况下不会触发，也不是这里讨论的焦点。 

举个例子：

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        #box{
            background: red;
            width: 300px;
            height: 300px;
        }
    </style>
</head>

<body>
    <div id="box">
    </div>
    <script>
        let box = document.getElementById('box');
        box.addEventListener('touchstart', function() {
            console.log('touchstart', +new Date())
        })
        box.addEventListener('touchmove', function() {
            console.log('touchmove', +new Date())
        })
        box.addEventListener('touchend', function() {
            console.log('touchend', +new Date())
        })
        box.addEventListener('click', function() {
            console.log('click', +new Date())
        })
        document.body.addEventListener('touchstart', function() {
            console.log('body touchstart', +new Date());
        })
        document.body.addEventListener('touchmove', function() {
            console.log('body touchmove', +new Date());
        })
        document.body.addEventListener('touchend', function() {
            console.log('body touchend', +new Date());
        })
        document.body.onclick = function() {
            console.log('body click', +new Date());
        }
    </script>
</body>

</html>
```

## 1.1 touch和click事件的执行顺序

上面的代码，在手机状态下，我们点击盒子时，控制台输出了：

![](images/touch/touch-01.png)

针对结果我们发现：

- `touchmove` 事件没有触发。这是因为我们没有移动。
- `touchend`事件执行后，会执行`click`事件在，并且有一定的时间延迟。

由此我们可以得出结论：

**当一个用户在点击屏幕的时候，系统会触发touch事件和click事件，touch事件优先处理，touch事件经过 捕获，处理, 冒泡 一系列流程处理完成后， 才回去触发click事件** 

## 1.2 touchmove 与 click 事件

我们再次点击盒子，并且移动了。结果：

![](images/touch/move.png)

我们发现：

- `touchmove`事件被触发了多次
- `click`事件没有触发了。**这是因为click事件执行的点击事件，不是移动。所以一般情况下，我们可以理解`touchmove`和`click`事件是互斥的。**

## 3. 禁用 click 事件

我们知道touch事件和click事件有了优先级别，那么能不能在touch阶段取消系统触发click事件呢？答案是可以的，浏览器提供了这样的能力。在touch事件里面，调用`e.preventDefault()`就可以阻止本次点击系统触发的click事件，即本次相关的click都不会执行。

``` js
box.addEventListener('touchstart', function(e) {
    console.log('touchstart', +new Date());
    e.preventDefault();
})
```

![](images/touch/touch-02.png)

点击盒子后，由输出结果，我们可以看出，click事件没有触发。



## 4. 事件穿透

```html
<style>
    .parent {
        position: relative;
    }
    #box1 {
        position: absolute;
        top: 0;
        left: 0;
        width: 300px;
        height: 300px;
        background-color: aquamarine;
    }
    #box2 {
        width: 100%;
        height: 400px;
        background-color: antiquewhite;
    }
</style>
<div class="parent">
    <div id="box1"></div>
    <div id="box2"></div>
</div>
<script>
    let box1 = document.getElementById('box1');
    let box2 = document.getElementById('box2');
    box1.addEventListener('touchstart', function() {
        console.log('box1 touchstart');
        box1.style.display = 'none';
    });

    box2.addEventListener('touchstart', function() {
        console.log('box2 touchstart')
    });

    box2.addEventListener('click', function() {
        console.log('box2 click')
    })
</script>
```

上面代码，**盒子1和盒子2是兄弟节点，它们之间不会发生什么事件传递。**当前，将盒子1通过定位覆盖在盒子2的上面。我们点击盒子1的时候，在它的`touchstart`事件触发时，将自己隐藏起来。然后我们发现盒子2的`click`事件被触发。这就是发生事件穿透。

**事件穿透发生的条件：**

1. A 和 B 不是后代继承关系（如果存在后代继承关系的话，就是冒泡之类的话题了）；
2. A 的`z-index` 大于 B，即 A 显示在 B 浮层之上
3. A 发生 `touch`，A 在 `touch` 后立即消失，B 绑定了 `click` 事件。

**事件穿透的原因：**

> 当手指触摸到屏幕的时候，系统生成了两个事件，一个是`touch`，一个是`click`。`touch`先执行， `click` 后面执行，执行`click`事件的时候，相对于`touch`事件有延时。`touch`执行之后，A 从文档树上消失了，系统再次执行 `click` 事件的时候，发现在用户点击的位置上，当前的元素是 B，所以就直接把 click 事件作用在 B 元素上面了。

**如何解决事件穿透的问题呢？**

- 前面介绍了，系统提供了先触发touch事件后，取消系统生成click事件的方法。我们只要在touch事件的某个处理函数中执行`e.preventDefault()`即可。

- 解决穿透问题，关键是可以破坏生成的条件。

  - 不在 B 上绑定click事件（如果业务需求不允许，则此方法不可行）

  - 即让click事件执行时，A 元素还存留来页面中。即可以设置延迟，一般是300ms。

    ```js
    setTimeout(function() {
        box1.style.display = 'none';
    }, 300)
    ```

    


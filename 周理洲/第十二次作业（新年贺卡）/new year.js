//在DOM加载完成后，执行js
window.addEventListener('DOMContentLoaded', function () {
    let music = document.querySelector('.music');
    let img = music.firstElementChild;
    let audio = document.querySelector('audio');
    let one = document.querySelector('.one');
    let two = document.querySelector('.two');
    let three = document.querySelector('.three');
    //判断音乐播放器是否在运行
    if (audio.paused) {
        img.classList.remove('only');
    } else {
        img.classList.add('only');
    }
    //控制音乐播放器
    music.addEventListener('touchstart', function () {
        if (audio.paused) {
            img.classList.add('only');
            audio.play();
            img.style.animationPlayState = 'running';
        } else {
            audio.pause();
            img.style.animationPlayState = 'paused';
        }
    })
    //音乐播放完暂停动画
    music.addEventListener('ended', function () {
        img.classList.remove('only');
    })
    //切换页面
    one.addEventListener('touchstart', function () {
        two.style.display = 'block';
        one.style.display = 'none';
        three.style.display = 'block';
        setTimeout(function () {
            two.classList.add('two-h');
            three.classList.add('three-h');
        }, 5000)
    })
})
// 判断用户名格式
function regName() {
    username = document.getElementById("username").value;
    var reg1 = /^[A-Za-z\u4E00-\u9Fa5]{5,35}$/;

    if (reg1.test(username)){

        document.getElementById("regName").innerText = "正确";
        document.getElementById("regName").style.color = "green"; 
    } else {
        document.getElementById("regName").innerText = "5-25字符，可以包含字母或者汉字";
        document.getElementById("regName").style.color = "red";
    }
}

//判断第一次密码
function regPwd() {
    password1 = document.getElementById("password1").value;
    var reg2 = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[~!@#$%^&*]).{8,16}$/;
    if (reg2.test(password1)){
        document.getElementById("regPwd").innerText = "正确";
        document.getElementById("regPwd").style.color = "green";
    } else {
        document.getElementById("regPwd").innerText = "密码设置不符合要求";
        document.getElementById("regPwd").style.color = "red";
    }
}

//判断确认密码
function regSurePwd() {
    password1 = document.getElementById("password1").value;
    password2 = document.getElementById("password2").value;
    if (password2 == password1){
        document.getElementById("regSurePwd").innerText = "正确";
        document.getElementById("regSurePwd").style.color = "green";
    } else {
        document.getElementById("regSurePwd").innerText = "两次密码不一致";
        document.getElementById("regSurePwd").style.color = "red";
            }
}

//判断姓名
function regPerName() {
    contactPerson = document.getElementById("contact-person").value;
    var reg3 = /^[\u4E00-\u9FA5\uf900-\ufa2d·s]{2,20}$/;
    if (reg3.test(contactPerson)){
        document.getElementById("regPerName").innerText = "正确";
        document.getElementById("regPerName").style.color = "green";
    } else {
        document.getElementById("regPerName").innerText = "联系人姓名不符合要求";
        document.getElementById("regPerName").style.color = "red";
    }
}

//判断企业名称
function regComName() {
    companyName = document.getElementById("company-name").value;
    var reg4 = /^[\u4e00-\u9fa5\(\)（）\da-zA-Z&]{2,50}$/;
    if (reg4.test(companyName)){
        document.getElementById("regComName").innerText = "正确";
        document.getElementById("regComName").style.color = "green";
    } else {
        document.getElementById("regComName").innerText = "企业姓名不符合要求";
        document.getElementById("regComName").style.color = "red";
    }
}

//判断手机号码
function regPhone() {
    phoneNumber = document.getElementById("phone-number").value;
    var reg5 = /^1[3-9]{2,10}$/;
    if (reg5.test(phoneNumber)){
        document.getElementById("regPhone").innerText = "正确";
        document.getElementById("regPhone").style.color = "green";
    } else {
        document.getElementById("regPhone").innerText = "手机号码不符合要求";
        document.getElementById("regPhone").style.color = "red";
    }
}

function varReg(){
    regName();
    regPwd();
    regSurePwd();
    regPerName();
    regComName();
    regPhone();
}

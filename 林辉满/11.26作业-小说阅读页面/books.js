window.onload = function() {
	let body = document.body;
	body.onclick = function() {
		let top = document.getElementById("top");
		if (top.style.display == "none") {
			top.style.animation = "top 0.5s linear forwards";
			top.style.display = "block"
		} else {
			top.style.animation = "top2 0.5s linear forwards";
			top.style.display = "none";
		}
		let bottom = document.getElementById("bottom");
		if (bottom.style.display == "none") {
			bottom.style.animation = "bottom 0.5s linear forwards";
			bottom.style.display = "block"
		} else {
			bottom.style.animation = "bottom2 0.5s linear forwards";
			bottom.style.display = "none";
		}
	}
}

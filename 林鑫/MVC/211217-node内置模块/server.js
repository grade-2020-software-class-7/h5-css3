var fs = require('fs');
var url = require('url');
var path = require('path');
var http = require('http');

let server = http.createServer(function (request, response) {
  var root = path.resolve('.');
  console.log(root);
  let pathname = url.parse(request.url).pathname;
  console.log(pathname);
  let filepath = path.join(root, pathname)
  console.log(filepath);
  fs.stat(filepath, function (err, stats) {
    if (err) {
      console.log('报错')
      response.writeHead('500');
      response.end();
    } else {
      if (stats.isFile()) {
        //文件系统读取文件信息
        fs.readFile(filePath, function(err, stats) {
          if (err) {
            //找不到文件，res返回一个404
            response.writeHead('404');
            response.end();
          } else {
            //文件存在，res返回文件信息和一个200
            response.writeHead(200);
            response.write(stats);
            response.end();
          }
        });
      }
      else if (stats.isDirectory()) {
        let indexflie = path.join(filepath, 'index.html')
        fs.readFile(indexflie, function (err, data) {
          if (err) {
            let defultpath = path.join(filepath,'default.html')
            fs.readFile(defultpath, function (err, data) {
              if (err) {
                response.writeHead('404');
                response.end();
              } else {
                response.writeHead(200)
                response.write(data)
                response.end()
              }
            })
          } else {
            //文件存在，res返回index.html文件信息和一个200
            response.writeHead(200);
            response.write(data);
            response.end();
          }
        })
      }
    }
  })
}).listen(8080, function () {
  console.log('Server is running at http://127.0.01:8080');
});

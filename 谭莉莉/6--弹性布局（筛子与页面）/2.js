$(function () {
    //切换至inline-flex
    $('#inline-flex').on('click', function () {
        $('.jiao').toggleClass('inline-flexs');
        if ($(this).text() == '切换至inline-flex') {
            $(this).html('切换至flex')
        } else {
            $(this).html('切换至inline-flex')
        }
    })
    //增加成员
    $('#zj').on('click', function () {
        let $jiao = $('.jiao')
        let $div = $('<div></div>')
        let $item = $('.item')
        $div.addClass('item')
        let u = $($item[$item.length - 1]).html();
        u++;
        $div.html(`${u}`);
        $jiao.append($div)
        let rand = parseInt(Math.random() * 9)
        if (rand < 4) {
            $div.attr('data-line', 'line1')
        } else { $div.attr('data-line', 'line3') }
    })
    //减少成员
    $('#js').on('click', function () {
        let $item = $('.item')
        let $u = $item[$item.length - 1]
        $u.remove();
    })
    //增加内联文本
    $('#zjnl').on('click', function () {
        let $span = $('.span')
        let $div = $('<span></span>')
        $div.html(`非内联flex`);
        $span.before($div)
    })
    $('.row').on('click', function () {
        let $jiao = $('.jiao')
        $jiao.css('flex-direction', $(this).val())
    })
    $('.nowrap').on('click', function () {
        let $jiao = $('.jiao')
        $jiao.css('flex-wrap', $(this).val())
    })
    $('.justify-content').on('click', function () {
        let $jiao = $('.jiao')
        $jiao.css('justify-content', $(this).val())
    })
    $('.align-items').on('click', function () {
        let $jiao = $('.jiao')
        $jiao.css('align-items', $(this).val())
    })
    $('.align-content').on('click', function () {
        let $jiao = $('.jiao')
        $jiao.css('align-content', $(this).val())
    })
    $('.row nowrap').on('click', function () {
        let $jiao = $('.jiao')
        $jiao.css('row nowrap', $(this).val())
    })
})
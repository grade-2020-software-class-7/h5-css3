window.onload = function () {
    let bycolor = document.getElementsByClassName('bycolor')
    for (let j = 0; j < bycolor.length; j++) {
        bycolor[j].onclick = function () {
            let color = this.value;
            document.body.style.backgroundColor = color
        }
    }

    let us = document.getElementsByTagName('u');
    for (let i = 0; i < us.length; i++) {
        us[i].onclick = function () {
            if (this.textContent == '特大') {
                document.body.style.fontSize = '30px'
            } else if (this.textContent == '大') {
                document.body.style.fontSize = '20px'
            } else if (this.textContent == '中') {
                document.body.style.fontSize = '15px'
            } else if (this.textContent == '小') {
                document.body.style.fontSize = '10px'
            }
        }
    }
    let topbox = document.querySelector('.top-box')
    let addbook = document.querySelector('.addbook')
    let write = document.querySelector('.write')
    let footerbox = document.querySelector('.footer-box')
    let article = document.querySelector('.article-conent')

    topbox.classList.add('yc')
    addbook.classList.add('yc')
    write.classList.add('yc')
    footerbox.classList.add('yc')
    let jl = 0;
    article.addEventListener('touchstart',function(){
        if (jl == 0) {
            topbox.classList.remove('yc')
            addbook.classList.remove('yc')
            write.classList.remove('yc')
            footerbox.classList.remove('yc')
            jl = 1
        } else {
            topbox.classList.add('yc')
            addbook.classList.add('yc')
            write.classList.add('yc')
            footerbox.classList.add('yc')
            jl = 0;
        }
    })
}

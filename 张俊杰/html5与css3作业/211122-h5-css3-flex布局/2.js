window.onload = function () {
    //先获取第一个点击元素
    let span1 = document.querySelector(".header-button")

    let div = document.querySelector(".body")

    span1.onclick = function () {
        if (span1.textContent == "切换至 inline-flex") {
            span1.innerText = "切换至 flex"
            div.style.display = "inline-flex"
        } else {
            span1.innerText = "切换至 inline-flex"
            div.style.display = "flex"
        }
    }

    let select = document.querySelectorAll(".header-select")

    select.forEach(function (n) {
        n.onchange = function () {
            if (this.firstElementChild.value == "flex-direction") {
                div.style.flexDirection = this.value
            } else if (this.firstElementChild.value == "flex-wrap") {
                div.style.flexWrap = this.value
            } else if (this.firstElementChild.value == "justify-content") {
                div.style.justifyContent = this.value
            } else if (this.firstElementChild.value == "align-items") {
                div.style.alignItems = this.value
            } else if (this.firstElementChild.value == "align-content") {
                div.style.alignContent = this.value
            } else if (this.firstElementChild.value == "flex-flow") {
                div.style.flexFlow = this.value
            }
        }
    })

    let button = document.querySelectorAll(".foot-button")

    //获取css中的值要先这样styleSheets[0],这个是端口,cssRules这个是css中所有的属性，是一个伪数组
    let css = document.styleSheets[0].cssRules[0].style
    let lineheight = css.getPropertyValue("--height")

    let ji = 0

    button[0].onclick = function () {
        let length = div.children.length
        let newdiv = document.createElement("div")
        newdiv.innerText = ++length

        //减到20就开始加
        if (ji == 0 && lineheight >= 30) {
            lineheight -= 10
            newdiv.style.lineHeight = lineheight + "px"
        }

        if(lineheight == 20){
            ji = 1
        }

        //加到100就开始减
        if (ji == 1 && lineheight <= 90) {
            lineheight += 10
            newdiv.style.lineHeight = lineheight + "px"
        }

        if(lineheight == 100){
            ji = 0
        }

        div.append(newdiv)
    }

    button[1].onclick = function () {
        div.removeChild(div.lastElementChild)
    }

    button[2].onclick = function () {
        let lastspan = document.querySelector("#lastspan")
        lastspan.prepend("xx内联脚本xx")
    }
}
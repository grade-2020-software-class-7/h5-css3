# CSS效果

## 本节目的

作为一名web开发者，css 是必备技能之一。本次课程我们介绍一下一些常用的css小样式。

## 1. 静态样式修改

### 1.1 清除默认样式

**重置** reset.css

```css
html,body,h1,h2,h3,h4,h5,h6,div,dl,dt,dd,ul,ol,li,p,blockquote,pre,hr,figure,table,caption,th,td,form,fieldset,legend,input,button,textarea,menu{margin:0;padding:0;}
header,footer,section,article,aside,nav,hgroup,address,figure,figcaption,menu,details{display:block;}
table{border-collapse:collapse;border-spacing:0;}
caption,th{text-align:left;font-weight:normal;}
html,body,fieldset,img,iframe,abbr{border:0;}
i,cite,em,var,address,dfn{font-style:normal;}
[hidefocus],summary{outline:0;}
li{list-style:none;}
h1,h2,h3,h4,h5,h6,small{font-size:100%;}
sup,sub{font-size:83%;}
pre,code,kbd,samp{font-family:inherit;}
q:before,q:after{content:none;}
textarea{overflow:auto;resize:none;}
label,summary{cursor:default;}
a,button{cursor:pointer;}
h1,h2,h3,h4,h5,h6,em,strong,b{font-weight:bold;}
del,ins,u,s,a,a:hover{text-decoration:none;}
body,textarea,input,button,select,keygen,legend{font:12px/1.14 arial,\5b8b\4f53;color:#333;outline:0;}
body{background:#fff;}
a,a:hover{color:#333;}
```

### 1.2 三角形

我们使用边框来绘制三角形：

```css
.triangle {
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 0 50px 100px 50px;
    border-color: transparent transparent #00adb5 transparent;
}
```

### 1.3 滚动条样式

```css
.scroll-container {
    height: 250px;
    border: 1px solid #ddd;
    padding: 15px;
    overflow: auto;
}
.scroll-container::-webkit-scrollbar {
    width: 8px;
    background: white;
}
.scroll-container::-webkit-scrollbar-corner,/* 动条角落 /
.scroll-container::-webkit-scrollbar-thumb,
.scroll-container::-webkit-scrollbar-track {
	border-radius: 4px;
}
.scroll-container::-webkit-scrollbar-corner,
.scroll-container::-webkit-scrollbar-track {
/* 滚动条轨道 */
	background-color: rgba(180, 160, 120, 0.1);
	box-shadow: 0 0 1px rgba(180, 160, 120, 0.5) inset;
}
.scroll-container::-webkit-scrollbar-thumb {
    /* 滚动条手柄 */
    background-color: #00adb5;
}
```

## 2. 动态样式

### 2.1 提示气泡

![](images/utils/pop.png)

当鼠标移动到元素上时，弹出和提示气泡。

```html
<div class="poptip btn" aria-controls="弹出气泡">气泡提示</div>
```

```css
:root {
    /* 定义气泡弹出框背景色 */
    --poptipBg: #30363d;
    /* 弹出框字体颜色 */
    --color: #fff;
    /* 弹出框三角形边长 */
    --triangle: 8px;
    /* 弹出框与元素的偏移 */
    --distance: -12px;
}
.poptip {
  position: relative;
  z-index: 101;
}
.poptip::before,
.poptip::after {
    visibility: hidden;
    opacity: 0;
    transform: translate3d(0, 0, 0);
    transition: all 0.3s ease 0.2s;
    box-sizing: border-box;
  }
.poptip::before {
    content: "";
    position: absolute;
    width: 0;
    height: 0;
    border-style: solid;
    border-width: var(--triangle) var(--triangle) 0 var(--triangle);
    border-color: var(--poptipBg) transparent transparent transparent;
    left: calc(50% - var(--triangle));
    top: 0px;
    transform: translateX(0%) translateY(var(--distance));
  }

.poptip::after {
    font-size: 14px;
    color: var(--color);
    /* 内容为标签内的属性值 */
    content: attr(aria-controls);
    position: absolute;
    padding: 6px 12px;
    white-space: nowrap;
    z-index: -1;
    left: 50%;
    bottom: 100%;
    transform: translateX(-50%) translateY(var(--distance));
    background: var(--poptipBg);
    line-height: 1;
    border-radius: 2px;
  }
.poptip:hover::before,
.poptip:hover::after {
    visibility: visible;
    opacity: 1;
}

.btn {
  width: 100px;
  line-height: 1.5;
  padding: 5px 10px;
  color: #fff;
  background: #00adb5;
  border-radius: 4px;
  text-align: center;
  cursor: pointer;
  margin: 200px auto;
}
```

### 2.2 手风琴

![](images/utils/accordion.png)

```html
<ul class="accordion-container">
    <li class="accordion-item" style="background-image: url(bg1.jpg);">
    </li>
    ···
</ul>
```

```css
.accordion-container {
    overflow: hidden;
    display: flex;
    justify-content: flex-start;
    align-items: flex-start;
}
.accordion-container > .accordion-item {
    list-style: none;
    height: 500px;
    transition: width 0.5s;
    background-repeat: no-repeat;
    background-size: 500px;
    background-position: center center;
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
}
.accordion-container > .accordion-item:not(:hover) {
    width: 20%;
}
.accordion-container > .accordion-item:hover {
    width: 281.20px;
}
.accordion-container > .accordion-item > .accordion-title {
    color: #fff;
    font-size: 18px;
    z-index: 2;
}
```

### 2.3 加载效果

#### 2.3.1 围绕一个圆轨道转

![](images/utils/loading1.png)

```html
 <div class="load"></div>
```

```css
.load {
  width: 50px;
  height: 50px;
  margin: 0 auto;
  position: relative;
  border-radius: 50%;
  overflow: hidden;
  background-color: rgba(0, 169, 178,.2);
}
.load::before {
    content: "";
    width: 50px;
    height: 50px;
    background-color: #00adb5;
    position: absolute;
    left: 50%;
    bottom: 50%;
    z-index: 1;
    transform-origin: left bottom;
    animation: rotate 1.5s infinite linear;
}
.load::after {
    content: "";
    width: 40px;
    height: 40px;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    margin: auto;
    background-color: #fff;
    z-index: 2;
    border-radius: 50%;
}
@keyframes rotate {
  0% {
    transform: rotate(0);
  }
  50% {
    transform: rotate(180deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
```

> 原理：
>
> 绘制一个同心圆，然后控制一个有背景色的矩形旋转。根据层级的关系来展示。

#### 2.3.2 声浪效果

![](images/utils/loading2.png)

```html
<div class="load-container">
  <div class="container">
      <div class="boxLoading boxLoading1"></div>
      <div class="boxLoading boxLoading2"></div>
      <div class="boxLoading boxLoading3"></div>
      <div class="boxLoading boxLoading4"></div>
      <div class="boxLoading boxLoading5"></div>
  </div>
</div>
```

```css
.load-container {
    height: 150px;
    display: flex;
    align-items: center;
    justify-content: center;
}
.load-container .container{
    width: 50px;
    height: 60px;
    text-align: center;
    font-size: 10px;
}
.boxLoading {
    background-color: #00adb5;
    height: 100%;
    width: 6px;
    display: inline-block;
    animation: stretchdelay 1.2s infinite ease-in-out;
}
.boxLoading2 {
    animation-delay: -1.1s;
}
.boxLoading3 {
    animation-delay: -1.0s;
}
.boxLoading4 {
    animation-delay: -0.9s;
}
.boxLoading5 {
    animation-delay: -0.8s;
}

@-webkit-keyframes stretchdelay {
  0%, 40%, 100% { -webkit-transform: scaleY(0.4) }
  20% { -webkit-transform: scaleY(1.0) }
}
@keyframes stretchdelay {
  0%, 40%, 100% {
    transform: scaleY(0.4);
    -webkit-transform: scaleY(0.4);
  } 
  20% {
    transform: scaleY(1.0);
    -webkit-transform: scaleY(1.0);
  }
}
```

> 上面代码`animation-delay`的值为负值，为什么这么设置？
>
> 取负值的效果：
>
> 1. 动画会立即开始执行
> 2. 开始的位置是动画的其中一个阶段
>
> animation-delay为正数时，动画就要延时开始，我们看到的动画就是从第一帧开始的；animation-delay为负数的时候，意味着动画是提前开始的，animation-time已经开始计算了，我们看到的动画是从某一帧开始的。



## 作业

1. 提示气泡优化，实现左边提示，右边提示，下面提示
window.onload = function () {
    let ranges = document.getElementsByClassName("range");
    let r = 255;
    let g = 255;
    let b = 255;
    for (let i = 0; i < ranges.length; i++) {
        ranges[i].addEventListener("input", function (event) {
            let name = event.target.name;
            if (name == "red") {
                let Value = event.target.value;
                let rgb = document.getElementById("R");
                rgb.innerHTML = Value;
                let bgColor = document.getElementById("rgb");
                bgColor.style.backgroundColor = "" + bgColor.innerText + "";
                r = Value;
            } else if (name == "green") {
                let Value = event.target.value;
                let rgb = document.getElementById("G");
                rgb.innerHTML = Value;
                let bgColor = document.getElementById("rgb");
                bgColor.style.backgroundColor = "" + bgColor.innerText + "";
                g = Value;
            } else if (name == "blue") {
                let Value = event.target.value;
                let rgb = document.getElementById("B");
                rgb.innerHTML = Value;
                let bgColor = document.getElementById("rgb");
                bgColor.style.backgroundColor = "" + bgColor.innerText + "";
                b = Value;
            } else if (name == "Red") {
                let Value = event.target.value;
                let rgb = document.getElementById("RR");
                rgb.innerHTML = Value;
                let bgColor = document.getElementById("rgba");
                bgColor.style.backgroundColor = "" + bgColor.innerText + "";
                r = Value;
            } else if (name == "Green") {
                let Value = event.target.value;
                let rgb = document.getElementById("GG");
                rgb.innerHTML = Value;
                let bgColor = document.getElementById("rgba");
                bgColor.style.backgroundColor = "" + bgColor.innerText + "";
                g = Value;
            } else if (name == "Blue") {
                let Value = event.target.value;
                let rgb = document.getElementById("BB");
                rgb.innerHTML = Value;
                let bgColor = document.getElementById("rgba");
                bgColor.style.backgroundColor = "" + bgColor.innerText + "";
                b = Value;
            } else if (name == "Opacity") {
                let Value = event.target.value;
                let rgb = document.getElementById("AA");
                rgb.innerHTML = Value;
                let bgColor = document.getElementById("rgba");
                bgColor.style.backgroundColor = "" + bgColor.innerText + "";
            } else if (name == "h") {
                let Value = event.target.value;
                let hsl = document.getElementById("h");
                hsl.innerHTML = Value;
                let bgColor = document.getElementById("hsl");
                bgColor.style.backgroundColor = "" + bgColor.innerText + "";
                r = Value;
            } else if (name == "s") {
                let Value = event.target.value;
                let hsl = document.getElementById("s");
                hsl.innerHTML = Value;
                let bgColor = document.getElementById("hsl");
                bgColor.style.backgroundColor = "" + bgColor.innerText + "";
                g = Value;
            } else if (name == "l") {
                let Value = event.target.value;
                let hsl = document.getElementById("l");
                hsl.innerHTML = Value;
                let bgColor = document.getElementById("hsl");
                bgColor.style.backgroundColor = "" + bgColor.innerText + "";
                b = Value;
            }else if (name == "H") {
                let Value = event.target.value;
                let hsla = document.getElementById("H");
                hsla.innerHTML = Value;
                let bgColor = document.getElementById("hsla");
                bgColor.style.backgroundColor = "" + bgColor.innerText + "";
                r = Value;
            } else if (name == "S") {
                let Value = event.target.value;
                let hsla = document.getElementById("S");
                hsla.innerHTML = Value;
                let bgColor = document.getElementById("hsla");
                bgColor.style.backgroundColor = "" + bgColor.innerText + "";
                g = Value;
            } else if (name == "L") {
                let Value = event.target.value;
                let hsla = document.getElementById("L");
                hsla.innerHTML = Value;
                let bgColor = document.getElementById("hsla");
                bgColor.style.backgroundColor = "" + bgColor.innerText + "";
                b = Value;
            }else if (name == "opacity") {
                let Value = event.target.value;
                let hsla = document.getElementById("A");
                hsla.innerHTML = Value;
                let bgColor = document.getElementById("hsla");
                bgColor.style.backgroundColor = "" + bgColor.innerText + "";
            }



            if (name == "red" || name == "green" || name == "blue") {
                let BgColor = document.getElementById("rgb");
                let color = (r * 0.2126 + g * 0.7152 + b * 0.0722) / 255;
                BgColor.style.color = "hsl(0,0%," + (color - 0.5) * -9999.99 + "%)";
            }else if(name == "Red" || name == "Green" || name == "Blue"){
                let BgColorA = document.getElementById("rgba");
                let color = (r * 0.2126 + g * 0.7152 + b * 0.0722) / 255;
                BgColorA.style.color = "hsl(0,0%," + (color - 0.5) * -9999.99 + "%)";
            }else if(name == "h" || name == "s" || name == "l"){
                let BgColor = document.getElementById("hsl");
                let color = (r * 0.2126 + g * 0.7152 + b * 0.0722) / 255;
                BgColor.style.color = "hsl(0,0%," + (color - 0.5) * -9999.99 + "%)";
            }
        });
    }
}
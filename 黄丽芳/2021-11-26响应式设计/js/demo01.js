$(function () {
    //全选点击
    $('.checkall').click(function () {
        $('.j-checkbox').prop('checked', $(this).prop('checked'))
        $('.checkall').prop('checked', $(this).prop('checked'))

    })
    //增加与减少
    $('.increment').click(function () {
        let $a = $(this).siblings('.itxt')
        let b = $a.val();
        b++;
        $a.val(b)

        //小计变化
        let $money = $(this).closest('.p-num').siblings('.p-price')
        let money = $money.text().substr(1, $money.text().length - 1);
        let sum = Number(money) * b
        $(this).closest('.p-num').siblings('.p-sum').text('￥' + sum.toFixed(2));
      
        getsum()
    })


    $('.decrement').click(function () {

        let $a = $(this).siblings('.itxt');
        let b = $a.val()
        if (b > 1) {
            b--;
            $a.val(b)
            //小计变化
            let $money = $(this).closest('.p-num').siblings('.p-price')
            let money = $money.text().substr(1, $money.text().length - 1);
            let sum = Number(money) * b
            $(this).closest('.p-num').siblings('.p-sum').text('￥' + sum.toFixed(2));
            getsum()
        }
    })
    function getsum() {
        let money = 0;
        let count = 0;
        $('.itxt').each(function (i, ele) {
            count += parseInt($(ele).val())
        })
        $('.amount-sum em').text(count)

        $('.p-sum').each(function (i, ele) {
            money +=parseFloat($(ele).text().substr(1)) 
        })
        $('.price-sum em').text("￥"+money.toFixed(2))
    }
})


window.onload = function () {
    document.body.onclick = function () {
        let nav = document.getElementById("nav");
        let bottom = document.getElementById("bottom");
        if(nav.style.display == "" || nav.style.display == "grid"){
            nav.style.display = "inline-grid";
            nav.style.animation = "navIn 0.5s ease-in-out  forwards"
            bottom.style.display = "inline-grid";
            bottom.style.animation = "bottomIn 0.5s ease-in-out  forwards"
        }else{
            nav.style.display = "grid";
            nav.style.animation = "navOut 0.5s ease-in-out  forwards"
            bottom.style.animation = "bottomOut 0.5s ease-in-out  forwards"
        }
    }
}
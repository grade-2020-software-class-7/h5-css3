window.addEventListener('DOMContentLoaded', function() {
  let music = document.getElementById('music');
  let audio = document.querySelector('audio');

  let page1 = document.getElementById('page1');
  let page2 = document.getElementById('page2');
  let page3 = document.getElementById('page3');
  // 兼容谷歌浏览器不能自动播放的问题
  if (audio.paused) {
    music.style.animationPlayState = 'paused';
  } else {
    music.style.animationPlayState = 'running';
  }
  // 点击音乐图标，控制音乐播放器效果
  music.addEventListener('touchstart', function() {
    if (audio.paused) {
      audio.play()
      // 当音乐停止后，会将play这个类移除掉，所以当音乐再次点击播放时，需要将这个类加上。
      music.classList.add('play');
      music.style.animationPlayState = 'running';
    } else {
      audio.pause()
      music.style.animationPlayState = 'paused';
    }
  })
  // 当音乐播放结束后，自动停止光盘旋转效果
  audio.addEventListener('ended', function() {
    music.classList.remove('play');
  });

  // 显示翻页效果
  page1.addEventListener('touchstart', function() {
    page1.style.display = 'none';
    page2.style.display = 'block';
    page3.style.display = 'block';
    page3.style.top = '100%';
    
    // 设置计时器，当第二个页面动画运行结束后，直接跳转到第三个页面
    setTimeout(() => {
      page2.classList.add('fade-out');
      page3.classList.add('fade-in')
    }, 5500);
  })
})
// 判断密码是否输入正确
window.addEventListener('DOMContentLoaded',function(){
    let form = document.forms.myform;
    let elements = form.elements;

    form.addEventListener('sumbit',function(e){
        // 阻止浏览器默认行为
        e.preventDefault()
        // 用户发起ajax请求
        ajax({
            url:this.action,
            method:'POST',
            data:`username=${elements.username.value}&password=${elements.password.value}`,
            
        })
    })
})
function ajax({url,method,success,data,error}){
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
        if(xhr.readyState===4){
            if(xhr.status===200){
                let res = JSON.parse(xhr.responseText)
                console.log(res);
                if(res.code==='200'){
                    success(res.data)
                }else{
                    error(res)
                }
            }else{
                alert(xhr.statusText)
            }
        }
    }
    xhr.open(method,url)
    xhr.setRequestHeader('Content-Tpye','application/x-www-form-urlencoded')
    xhr.send(data||null)
}
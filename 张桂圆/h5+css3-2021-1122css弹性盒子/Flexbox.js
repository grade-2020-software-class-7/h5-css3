// window.onload = function(){
//     //获取button
//     let button =document.querySelector('.button')
//     let divs = document.querySelector('.footer')
//     button.addEventListener('click',function(){
//         if(button.textContent=='切换至inline-flex'){
//             button.innerHTML='切换至flex';
//             divs.style.display ='inline-flex'
//         }else{
//             button.innerHTML='切换至inline-flex';
//             divs.style.display ='flex'
//         }
//     })
//     let selects=document.querySelectorAll('select')
//     selects.forEach(function(ele){
//         ele.onchange = function(){
//             if(this.firstElementChild.value=='flex-direction'){
//                 divs.style.flexDirection=this.value
//             }else if(this.firstElementChild.value=='flex-wrap'){
//                 divs.style.flexWrap=this.value
//             }else if(this.firstElementChild.value=='justify-content'){
//                 divs.style.justifyContent=this.value
//             }else if(this.firstElementChild.value=='align-items'){
//                 divs.style.alignItems=this.value
//             }else if(this.firstElementChild.value=='align-content'){
//                 divs.style.alignContent=this.value
//             }else if(this.firstElementChild.value=='flex-flow'){
//                 divs.style.flexFlow=this.value
//             }
//         }
//     })
// }
window.onload = function () {
    let button = document.querySelector('.button')
    let divs = document.querySelector('.footer')
    button.addEventListener('click', function () {
        if (button.textContent == '切换至inline-flex') {
            button.innerHTML = '切换至flex';
            divs.style.display = 'inline-flex';
        } else {
            button.innerHTML = '切换至inline-flex';
            divs.style.display = 'flex'
        }
    })
    let selects = document.querySelectorAll('select')
    selects.forEach(function (ele) {
        ele.onchange = function () {
            if (this.firstElementChild.value == 'flex-direction') {
                divs.style.flexDirection = this.value
            } else if (this.firstElementChild.value == 'flex-wrap') {
                divs.style.flexWrap = this.value
            } else if (this.firstElementChild.value == 'justify-content') {
                divs.style.justifyContent = this.value
            } else if (this.firstElementChild.value == 'align-items') {
                divs.style.alignItems = this.value
            } else if (this.firstElementChild.value == 'align-content') {
                divs.style.alignContent = this.value
            } else if (this.firstElementChild.value == 'flex-flow') {
                divs.style.flexFlow = this.value
            }
        }
    })
}
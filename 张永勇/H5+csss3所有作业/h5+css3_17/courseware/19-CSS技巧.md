# CSS 技巧

本节介绍一些项目中常用的处理布局的技巧。

## 1. 垂直居中

### 法一： absolute定位 + tansform：translateY(-50%)

```css
.parent {
    position: relative;
}
/* 方法一：transform：translateY(-50%) */
.child {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
}
```

### 法二：absolute定位 + margin：auto

```css
.parent {
  position: relative;
}
.child {
  position: absolute;
  top: 0;
  bottom: 0;
  margin: auto;
}
```

### 法三：flex布局，align-items: center

```css
.parent {
  display: flex;
  align-items: center;
}
```

## 2. 文本超出显示省略号

### 单行文本

```css
.article-container {
  /*宽度是必须的*/
  width: 500px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
```

### 多行文字

```css
.article-container {
  display: -webkit-box;
  word-break: break-all;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 4; //需要显示的行数
  overflow: hidden;
  text-overflow: ellipsis;
}
```

> 主要属性是：`-webkit-line-clamp`。这个属性当前除了IE浏览器，其他浏览器兼容性都挺好。
>
> **`-webkit-line-clamp`**属性可以把块容器中的内容限制为指定的行数。它只有在`display: -webkit-box`或者`display: -webkit-inline-box`，并且`-webkit-box-orient: vertical`时，才有效果。大多数情况下，我们还需要将`overflow`属性设置为`hidden`，否则内容不会被裁减。

## 3. 圣杯布局

此布局一般的需求为两边等宽，中间自适应的三栏布局。

```html
<div class="main">
  <div class="left"></div>
  <div class="center"></div>
  <div class="right"></div>
</div>
```

### 旧方法：float布局（不推荐）

```css
/* 父元素清楚浮动，伪元素方式 */
.main::after {
    content: "";
    clear: both;
}
.main > div {
    float: left;
    height: 250px;
    background-color: #b4a078;
    color: white;
}
.main .left,
.main .right {
    /* 左右两栏固定宽度 */
    width: 119px;
}
.main .center {
    background-color: aquamarine;
    width: calc(100% - 119px * 2);
}
```

### 新处理方式： flex 布局

```css
.main {
    display: flex;
    height: 250px;
}
.left,
.right {
    flex: 0 0 119px; 
    background-color: #b4a078;
}
.center {
    flex: 1;
    background-color: aquamarine;
}
```

### grid布局

```css
.main {
    display: grid;
    grid-template-columns: 119px 1fr 119px;
    column-gap: 10px
}
.left,
.right {
    background-color: #b4a078;
}
.center {
    background-color: aquamarine;
}
```

## 4. 全背景下等宽内容居中

![](images/utils/fluid_fixed.png)

每个盒子宽度都是100%，现在将里面的内容在固定宽度中展示，并且缩放盒子时，内容一直是居中的。

```html
<main class="main">
	<header>
    	<h2 class="title">我是标题</h2>
    </header>
    <section>
    	<p>
            ...
        </p>
    </section>
    <footer>
    	<p>&copy; 2021 Daisy</p>
        <p>CSS Tricks need to know for web developer.</p>
    </footer>
</main>
```

以前，我们的做法是将盒子固定宽度，然后通过`margin: 0 auto`来使得盒子能够在页面上水平居中。但是这种方式的话，盒子的背景颜色不能够撑满父盒子的宽度。

当前这种情况，我们的处理思路是：**padding + calc()**来处理。

> 左右内边距 = （父元素宽度 - 展示内容固定宽度） / 2；

假设中间内容区域宽度为： 900px。

```css
.main {
    width: 100%;
}
.main > header,
.main > section,
.main > footer {
    padding: 0 calc(50% - 450px);
}
```

## 5. 自定义单选框

浏览器自定义个单选框样式大多时候，不符合我们的业务需求，自定义单选框的使用场景很多。

![](images/utils/input-radio.png)

我们需要考虑的状态有：

- 正常选中状态
- 正常未选中状态
- 禁用未选中状态
- 禁用选中状态

如下结构：

```html
<div>
    <input type="radio" id="radio0" name="radio">
    <label for="radio0">Vue</label>
    <input type="radio" id="radio1" name="radio" checked>
    <label for="radio1">React</label>
    <input type="radio" id="radio3" name="radio">
    <label for="radio3">Angular</label>
    <input type="radio" id="radio4" name="radio" disabled>
    <label for="radio4">禁用</label>
    <input type="radio" id="radio5" name="radio" disabled class="checked">
    <label for="radio5">选中禁用</label>
</div>
```

思路：

1. `input`标签是单标签，它的内容不能够放置伪元素。原生样式的修改不容易，且不容易设置为我们想要的效果。所以我们将input元素隐藏，然后使用`<label>`标签`for`属性，来控制选中单选框。

   ```css
   input[type="radio"] {
       display: none;
   }
   ```

2. `input`标签隐藏后，我们要的样式就可以依赖`label`标签来实现。

3. `label`标签通过添加伪元素来实现效果。

   ```css
   input[type="radio"] + label {
       /* 控制内部元素可以垂直居中 */
       display: inline-flex;
       align-items: center;
       position: relative;
       cursor: pointer;
       /* 禁止用户选择文本 */
       user-select: none;
   }
   ```

4. 我们的单选框在选中状态时，有一个圆弧轨道。这种效果，我们可以通过设置同心圆来实现。

   ```css
   /* 在文本前面添加一个带有边框颜色的圆 */
   input[type="radio"] + label::before {
       content: "";
       display: inline-block;
       width: 10px;
       height: 10px;
       border-radius: 50%;
       margin-right: 0.2rem;
       border: 1px solid #ccc;
       background-color: #fff;
   }
   /* 绝对定位设置一个同心圆 */
   input[type="radio"] + label::after {
       content: "";
       position: absolute;
       width: 4px;
       height: 4px;
       left: 4px;
       top: calc(50% - 2px);
       background-color: #fff;
       border-radius: 50%;
   }
   ```

5. 接下来我们要处理选中状态下这些伪元素的样式。选中状态，我们可以通过`input:checked`这个伪类来匹配被选中的单选框。

   ```css
   input[type="radio"]:checked + label::before {
       background-color: #b4a078;
       border-color: #b4a078;
   }
   ```

6. 选项有个hover效果，边框的颜色会变色。

   ```css
   input[type="radio"] + label:hover::before {
       border-color: #b4a078;
   }
   ```

   上面代码生效的时候，页面上的禁用的单选框也出现了效果，所以，我们需要设置的是非禁用状态下的情况：

   ```css
   input[type="radio"]:not(:disabled) + label:hover::before {
       border-color: #b4a078;
   }
   ```

7. 禁用状态下的样式处理。首先在鼠标的手势上，我们做些修改。然后虚拟选择框的背景色也需要修改。禁用状态，我们使用伪类`:disabled`来匹配input元素。

   ```css
   input[type="radio"]:disabled + label {
       cursor: not-allowed;
       color: #999;
   }
   input[type="radio"]:disabled + label::before {
       background-color: #f2f2f2;
   }
   input[type="radio"]:disabled + label::after {
       background-color: #f2f2f2;
   }
   ```

   禁用状态下，选中元素的样式也不一样。这个时候，我们发现，我们需要用两个伪类作用在input元素上。可以像下面的写法：

   ```css
   input[type="radio"]:disabled:checked + label::before {
       border-color: #ccc;
   }
   input[type="radio"]:disabled:checked + label::after {
       background-color: #ccc;
   }
   ```

   > 这样子的写法有时会出现一些意想不到的问题。推荐使用js方式，给元素添加一个类来处理。

   纯css的写好还可以用下面方式：

   ```css
   input[type="radio"]:disabled:is(:checked) + label::before {
       border-color: #ccc;
   }
   input[type="radio"]:disabled:is(:checked) + label::after {
       background-color: #ccc;
   }
   ```

   > `:is()`方法，有兼容性的问题。

### 5.1 修改结构法

上面的例子是直接在`label`标签内，添加伪元素来实现这个方法。我们也可以转换思路，修改页面结构的方式来实现相同的功能。

```html
<div class="radio-box">
    <input type="radio" name="radio" id="radio1">
    <span class="radio"></span> 
    <label for="radio1">vue</label>
</div>
...
```

在这种结构里面，我们也是将input元素隐藏，然后使用`span.radio`来实现自定义单选框的样式。这样子的样式处理，我们常常会需要搭配js帮我添加表示状态的类。

实现的方式根据每个人的理解，可能都会有所不同。我们在一开始无法实现最优解时，我们只需要将自己能够实现的方式写出来就可以。

## 6. 自定义复选框

![](images/utils/input-checkbox.png)

HTML 结构如下：

```html
<main>
	<input type="checkbox" id="awesome1" checked>
    <label for="awesome1">Awesome</label>
    <input type="checkbox" id="awesome11">
    <label for="awesome11">Iconfont</label>
    <input type="checkbox" id="awesome12">
    <label for="awesome12">Lhammer</label>
    <input type="checkbox" id="awesome2" disabled>
    <label for="awesome2">禁用</label>
    <input type="checkbox" id="awesome3" disabled class="checked">
    <label for="awesome3">选中禁用</label>
</main>
```

基本操作和`radio`是一样的。差别在于图形中间的勾选样式不一致。

可以使用的方式：

- 这个勾选图案可以使用iconfont，添加一个字体图标到`label`的`after`伪元素的`content`属性中。

- 使用unicode来展示打钩的图形。

- 还有一种便捷方式，使用输入软件，打印一个 `√` 的图片。

- 使用一个边框，然后旋转这个边框也可以实现一个勾选效果。

  ```css
  label::after {
      content: "";
      width: 3px;
      height: 7px;
      border: 1px solid #fff;
      border-top: 0;
      border-left: 0;
      transform: rotate(45deg);
  }
  ```

- 选中状态的时候，这个勾选图标才显示。我们除了控制display属性外，我们还可以通过`scale(0)`将元素缩小最小即隐藏起来，选中状态时`scale(1)`。

## 作业

实现开关效果。

![](images/utils/close-open.png)

点击按钮后，会有过渡效果实现切换。

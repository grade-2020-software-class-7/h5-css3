
    // Controller
    class Controller {
      constructor(name) {
        this.name = name
      }

      _getPropertypeSupported(propertype) {
        if (propertype in document.body.style) {
          return propertype
        }
        var prefixes = ["Moz", "Webkit", "O", "ms", "Khtml"]
        var prefPropertype =
          propertype.charAt(0).toUpperCase() + propertype.substring(1)
        for (let i = 0; i < prefixes.length; i++) {
          if (prefixes[i] + prefPropertype in document.body.style) {
            return prefixes[i] + prefPropertype
          }
        }
        alert(`${propertype} 属性不支持, 用其他浏览器试试`)
      }

      _camelize(style) {
        return style.replace(/-(\w)/, function (all, letter) {
          return letter.toUpperCase()
        })
      }

      _getStyle(ele, propertype) {
        propertype = this._getPropertypeSupported(propertype)
        if (window.getComputedStyle) {
          return window.getComputedStyle(ele, null)[propertype]
        } else {
          if (propertype === "opacity") {
            return getIEOpacity(ele)
          }
          if (propertype === float) {
            return ele.currentStyle.getAttribute("styleFloat")
          }
          if (
            (propertype === "width" || propertype === "height") &&
            elem.currentStyle[propertype] == "auto"
          ) {
            var clientRect = ele.getBoundingClientRect()
            return (
              (propertype == "width" ?
                clientRect.right - clientRect.left :
                clientRect.bottom - clientRect.top) + "px"
            )
          }
          return ele.currentStyle.getAttribute(this._camelize(propertype))
        }
      }

      _setStyle(ele, propertype, value) {
        propertype = this._getPropertypeSupported(propertype)
        if (propertype == "opacity") {
          if (!ele.currentStyle || !elem.currentStyle.hasLayout) {
            ele.style.zoom = 1
          }
          propertype = "filter"
          if (!!window.XDomainRequest) {
            value =
              "progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=" +
              value * 100 +
              ")"
          } else {
            value = "alpha(opacity=" + value * 100 + ")"
          }
        }
        ele.style[propertype] = value
      }

      _minxinPropertype(...propertypes) {
        if (propertypes.length === 0) {
          return []
        }

        function minxinPropertype(propertype1 = [], propertype2 = []) {
          let newPropertype = []
          let newIndex = 0
          for (const propertypeValue1 of propertype1) {
            for (const propertypeValue2 of propertype2) {
              newPropertype[newIndex++] =
                propertypeValue1 + " " + propertypeValue2
            }
          }
          return newPropertype
        }

        return propertypes.reduce((propertype1, propertype2) => {
          return minxinPropertype(propertype1, propertype2)
        })
      }

      _createControlInput(ele, propertype) {
        const li = document.createElement("li")
        const label = document.createElement("label")
        const input = document.createElement("input")
        let value = this._getStyle(ele, propertype)
        input.value = value
        label.textContent = propertype + " :"
        li.classList.add("contorler-input")
        li.appendChild(label)
        li.appendChild(input)
        return li
      }

      _createControlNumberInput(ele, propertype) {
        const li = document.createElement("li")
        const label = document.createElement("label")
        const input = document.createElement("input")
        let value = this._getStyle(ele, propertype)
        let match = value.toString().match(/([-.\d]*)(.*)/)
        input.type = "number"
        if (!match[1]) {
          input.type = "text"
          input.value = match[2]
        } else {
          input.value = match[1]
        }
        let units = ["auto", "%", "em", "px", "rem", "ex"]
        label.textContent = propertype + " :"
        li.classList.add("contorler-number-input")
        li.appendChild(label)
        li.appendChild(input)
        if (match[2]) {
          const select = document.createElement("select")
          for (const unit of units) {
            const option = document.createElement("option")
            option.value = unit
            option.text = unit
            if (unit === match[2]) {
              option.selected = true
            }
            select.appendChild(option)
          }
          li.appendChild(select)
        }
        return li
      }

      _createControlButton(ele, propertype) {
        let str = "切换至 "
        let propertypeValue = ""
        let propertypeValues = this[propertype]
        let currentPropertypeValue = this._getStyle(ele, propertype)
        if (currentPropertypeValue === propertypeValues[0]) {
          propertypeValue = propertypeValues[1]
        } else {
          propertypeValue = propertypeValues[0]
        }
        const span = document.createElement("span")
        span.innerHTML = str + propertypeValue
        span.classList.add("contorler-button")
        return span
      }
      _createControlSelect(ele, propertype) {
        let propertypeValues = this[propertype]
        const select = document.createElement("select")
        select.classList.add("contorler-select", propertype)
        let prefix = propertype.replace(/([A-Z])/, (all, letter) => {
          return "-" + letter.toLowerCase()
        })
        select.title = prefix
        const option = document.createElement("option")
        option.text = prefix
        select.appendChild(option)
        for (const propertypeValue of propertypeValues) {
          const option = document.createElement("option")

          option.value = propertypeValue
          option.text = propertypeValue
          if (propertypeValue === this._getStyle(ele, propertype)) {
            setTimeout(() => {
              option.selected = true
            }, 1000)
          }
          setTimeout(() => {
            select.options[0].disabled = true
          }, 1000)
          select.appendChild(option)
        }
        return select
      }
    }

    // ContainerController
    class ContainerController extends Controller {
      constructor(
        wrapper, {
          name = "container-controller",
          ele = ".container",
          display = ["inline-flex", "flex"],
          flexDirection = ["row", "row-reverse", "column", "column-reverse"],
          flexWrap = ["nowrap", "wrap", "wrap-reverse"],
          justifyContent = [
            "flex-start",
            "flex-end",
            "center",
            "space-between",
            "space-around",
            "space-evenly"
          ],
          alignItems = [
            "stretch",
            "flex-start",
            "flex-end",
            "center",
            "baseline"
          ],
          alignContent = [
            "flex-start ",
            "flex-end",
            "center",
            "space-between",
            "space-around",
            "stretch"
          ]
        } = {}
      ) {
        super(name)
        this.wrapper = wrapper
        this.ele = this.wrapper.querySelector(ele)
        this.display = display
        this.flexDirection = flexDirection
        this.flexWrap = flexWrap
        this.justifyContent = justifyContent
        this.alignItems = alignItems
        this.alignContent = alignContent
        this.flexFlow = this._minxinPropertype(
          this.flexDirection,
          this.flexWrap
        )
      }

      _createControlAddItem(ele) {
        const span = document.createElement("span")
        span.innerHTML = "增加成员"
        span.classList.add("contorler-button")
        span.onclick = () => {
          const item = document.createElement("div")
          item.classList.add("item")
          item.innerHTML = ele.children.length + 1
          let randomLineHeight = Math.random() * 80 + 20
          this._setStyle(item, "line-height", randomLineHeight + "px")
          ele.appendChild(item)
        }
        return span
      }

      _createControlRemoveItem(ele) {
        const span = document.createElement("span")
        span.innerHTML = "减少成员"
        span.classList.add("contorler-button")
        span.onclick = () => {
          if (ele.lastElementChild) {
            ele.removeChild(ele.lastElementChild)
          }
        }
        return span
      }

      _createControlAddInlineText(ele) {
        const span = document.createElement("span")
        span.innerHTML = "增加内联文本"
        span.classList.add("contorler-button")
        span.onclick = () => {
          ele.insertAdjacentText("afterend", "xx内联脚本xx")
        }
        return span
      }

      _getControledButton(ele, propertype) {
        const button = this._createControlButton(ele, propertype)
        let propertypeValues = this[propertype]
        button.onclick = () => {
          let currentPropertypeValue = this._getStyle(ele, propertype)
          let prevPropertypeValue =
            currentPropertypeValue === propertypeValues[0] ?
            propertypeValues[1] :
            propertypeValues[0]
          button.innerHTML = button.innerHTML.replace(
            prevPropertypeValue,
            currentPropertypeValue
          )
          this._setStyle(ele, propertype, prevPropertypeValue)
        }
        return button
      }

      _getControledSelect(ele, propertype) {
        const select = this._createControlSelect(ele, propertype)
        select.onchange = e => {
          const currentTarget = e.currentTarget
          let propertypeValue =
            currentTarget.options[currentTarget.selectedIndex].value
          this._setStyle(ele, propertype, propertypeValue)
          const parentnode = currentTarget.parentNode
          const selectFlexFlow = parentnode.getElementsByClassName(
            "flexFlow"
          )[0]
          const selectFlexDirection = parentnode.getElementsByClassName(
            "flexDirection"
          )[0]
          const selectFlexWrap = parentnode.getElementsByClassName(
            "flexWrap"
          )[0]
          if (currentTarget.classList.contains("flexDirection")) {
            for (const option of selectFlexFlow.options) {
              if (
                option.value ===
                propertypeValue +
                " " +
                selectFlexWrap.selectedOptions[0].value
              ) {
                option.selected = true
              }
            }
          }

          if (currentTarget.classList.contains("flexWrap")) {
            for (const option of selectFlexFlow.options) {
              if (
                option.value ===
                selectFlexDirection.selectedOptions[0].value +
                " " +
                propertypeValue
              ) {
                option.selected = true
              }
            }
          }
          if (currentTarget.classList.contains("flexFlow")) {
            for (const option of selectFlexWrap.options) {
              if (
                option.value ===
                selectFlexFlow.selectedOptions[0].value.split(" ")[1]
              ) {
                option.selected = true
              }
            }
            for (const option of selectFlexDirection.options) {
              if (
                option.value ===
                selectFlexFlow.selectedOptions[0].value.split(" ")[0]
              ) {
                option.selected = true
              }
            }
          }
        }
        return select
      }

      render(
        propertypes = [
          "display",
          "flexDirection",
          "flexWrap",
          "justifyContent",
          "alignItems",
          "alignContent",
          "flexFlow"
        ]
      ) {
        const div = document.createElement("div")
        div.classList.add(this.name)
        for (const propertype of propertypes) {
          let ele
          let propertypeValues = this[propertype]
          if (propertypeValues.length === 2) {
            ele = this._getControledButton(this.ele, propertype)
          } else {
            ele = this._getControledSelect(this.ele, propertype)
          }
          div.appendChild(ele)
        }
        div.appendChild(this._createControlAddItem(this.ele))
        div.appendChild(this._createControlRemoveItem(this.ele))
        div.appendChild(this._createControlAddInlineText(this.ele))
        this.wrapper.insertBefore(div, this.wrapper.firstElementChild)
      }
    }

    // ItemController
    class ItemController extends Controller {
      constructor({
        name = "item-controller",
        alignSelf = [
          "stretch",
          "flex-start",
          "flex-end",
          "center",
          "baseline"
        ],
        numberInputPropertypes = [
          "order",
          "flex-grow",
          "flex-shrink",
          "flex-basis"
        ],
        inputPropertypes = ["flex"]
      } = {}) {
        super(name)
        this.alignSelf = alignSelf
        this.numberInputPropertypes = numberInputPropertypes
        this.inputPropertypes = inputPropertypes
      }

      _getControledSelect(ele, propertype) {
        const select = this._createControlSelect(ele, propertype)
        select.onchange = e => {
          const currentTarget = e.currentTarget
          let propertypeValue =
            currentTarget.options[currentTarget.selectedIndex].value
          this._setStyle(ele, propertype, propertypeValue)
        }
        return select
      }

      _updateFlex(ele, e) {
        const currentTarget = e.currentTarget
        const li = currentTarget.parentNode
        const itemController = li.parentNode
        if (
          li.classList.contains("flex-basis") ||
          li.classList.contains("flex-shrink") ||
          li.classList.contains("flex-grow")
        ) {
          const flex = itemController.getElementsByClassName("flex")[0]
          const input = flex.getElementsByTagName("input")[0]
          input.value = this._getStyle(ele, "flex")
        }
      }

      _getControledNumberInput(ele, propertype) {
        const li = this._createControlNumberInput(ele, propertype)
        li.classList.add(propertype)
        const input = li.getElementsByTagName("input")[0]
        if (
          propertype === "flex-grow" ||
          propertype === "flex-shrink" ||
          propertype === "flex-basis"
        ) {
          input.min = 0
        }
        let unit = "px"
        if (propertype === "flex-basis") {
          const select = li.getElementsByTagName("select")[0]
          select.onchange = e => {
            if (e.currentTarget.value !== "auto") {
              input.type = "number"
              input.value = input.value ? input.value : 0
              unit = e.currentTarget.value
              this._setStyle(ele, propertype, input.value + unit)
            } else {
              input.type = "text"
              input.value = "auto"
              this._setStyle(ele, propertype, "auto")
            }
            this._updateFlex(ele, e)
          }
        }
        input.oninput = e => {
          const currentTarget = e.currentTarget
          let value = currentTarget.value
          if (propertype === "flex-basis") {
            this._setStyle(ele, propertype, value + unit)
          } else {
            this._setStyle(ele, propertype, value)
          }
          this._updateFlex(ele, e)
        }
        return li
      }

      _getControledInput(ele, propertype) {
        const li = this._createControlInput(ele, propertype)
        li.classList.add(propertype)
        const input = li.getElementsByTagName("input")[0]
        if (propertype === "flex") {
          input.onchange = e => {
            let value = e.currentTarget.value
            this._setStyle(ele, propertype, value)
            const parentNode = li.parentNode
            let flexBasis = parentNode.getElementsByClassName("flex-basis")[0]
            let flexGrow = parentNode.getElementsByClassName("flex-grow")[0]
            let flexShrink = parentNode.getElementsByClassName(
              "flex-shrink"
            )[0]
            flexGrow.getElementsByTagName("input")[0].value = this._getStyle(
              ele,
              "flex-grow"
            )
            flexShrink.getElementsByTagName(
              "input"
            )[0].value = this._getStyle(ele, "flex-shrink")
            let flexBasisProperTypeValue = this._getStyle(ele, "flex-basis")
            let flexBasisInput = flexBasis.getElementsByTagName("input")[0]
            let flexBasisSelect = flexBasis.getElementsByTagName("select")[0]
            let match = flexBasisProperTypeValue
              .toString()
              .match(/([.\d]*)(.*)/)
            if (match[2] === "auto") {
              flexBasisInput.type = "text"
              flexBasisInput.value = "auto"
              flexBasisSelect.value = "auto"
            } else {
              flexBasisInput.type = "number"
              flexBasisSelect.value = match[2]
              flexBasisInput.value = match[1]
            }
          }
        }
        return li
      }

      render(propertypes = ["alignSelf"]) {
        const div = document.createElement("div")
        div.classList.add(this.name)
        for (const numberInputPropertype of this.numberInputPropertypes) {
          div.appendChild(
            this._getControledNumberInput(this.ele, numberInputPropertype)
          )
        }
        for (const inputPropertype of this.inputPropertypes) {
          div.appendChild(this._getControledInput(this.ele, inputPropertype))
        }
        for (const propertype of propertypes) {
          let ele
          let propertypeValues = this[propertype]
          ele = this._getControledSelect(this.ele, propertype)
          div.appendChild(ele)
        }
        return div
      }
    }

    const wrappers = document.getElementsByClassName("wrapper")
    for (const wrapper of wrappers) {
      const containerController = new ContainerController(wrapper)
      containerController.render()
    }
    const itemController = new ItemController()

    function itemControlHandler(e) {
      const target = e.target
      if (!target.classList.contains("item")) {
        return false
      }
      itemController.ele = target
      const div = itemController.render()
      let pageX = e.pageX
      let pageY = e.pageY
      let doc = document.documentElement
      let clientHeight = doc.clientHeight
      let clientWidth = doc.clientWidth
      itemController._setStyle(div, "left", pageX - 10 + "px")
      itemController._setStyle(div, "top", pageY - 10 + "px")
      document.body.appendChild(div)
      let boundingClientRect = div.getBoundingClientRect()
      if (boundingClientRect.right > clientWidth) {
        let left = parseFloat(itemController._getStyle(div, "left"))
        let x = boundingClientRect.right - clientWidth
        itemController._setStyle(div, "left", left - x + "px")
      }
      if (boundingClientRect.bottom > clientHeight) {
        let top = parseFloat(itemController._getStyle(div, "top"))
        let y = boundingClientRect.bottom - clientHeight
        itemController._setStyle(div, "top", top - y + "px")
      }
      let isLeave = false
      div.onmouseleave = function (e) {
        isLeave = true
        setTimeout(() => {
          if (isLeave) {
            div.parentNode.removeChild(div)
          }
        }, 500)
      }
      div.onmouseenter = function (e) {
        isLeave = false
      }
      div.onmousedown = function (e) {
        let rectBounding = div.getBoundingClientRect()
        let x = e.clientX - rectBounding.left
        let y = e.clientY - rectBounding.top
        div.onmousemove = function (e) {
          let pageX = e.pageX
          let pageY = e.pageY
          itemController._setStyle(div, "top", pageY - y + "px")
          itemController._setStyle(div, "left", pageX - x + "px")
        }
        div.onmouseup = function (e) {
          div.onmousemove = null
        }
      }
    }

    function itemControlTouchHandler(e) {
      const target = e.target
      if (!target.classList.contains("item")) {
        return false
      }
      itemController.ele = target
      const div = itemController.render()
      const touche = e.touches[0]
      let pageX = touche.pageX
      let pageY = touche.pageY
      let doc = document.documentElement
      let clientHeight = doc.clientHeight
      let clientWidth = doc.clientWidth
      itemController._setStyle(div, "left", pageX - 10 + "px")
      itemController._setStyle(div, "top", pageY - 10 + "px")
      document.body.appendChild(div)
      let boundingClientRect = div.getBoundingClientRect()
      if (boundingClientRect.right > clientWidth) {
        let left = parseFloat(itemController._getStyle(div, "left"))
        let x = boundingClientRect.right - clientWidth
        itemController._setStyle(div, "left", left - x + "px")
      }
      if (boundingClientRect.bottom > clientHeight) {
        let top = parseFloat(itemController._getStyle(div, "top"))
        let y = boundingClientRect.bottom - clientHeight
        itemController._setStyle(div, "top", top - y + "px")
      }
      let isLeave = true

      function leaveBySeconds(time = 2000) {
        setTimeout(() => {
          if (isLeave) {
            div.parentNode.removeChild(div)
          }
        }, time)
      }
      leaveBySeconds()
      
      for (const input of div.getElementsByTagName('input')) {
        input.onblur = function () {
          isLeave = true
          leaveBySeconds(4000)
        }
        input.onfocus = function () {
          isLeave = false
          return false
        }
      }
      for (const select of div.getElementsByTagName('select')) {
        select.addEventListener('focus',function () {
          isLeave = false
          return false
        },false)
        select.onblur = function () {
          isLeave = true
          leaveBySeconds(4000)
        }
         
      }

      div.ontouchstart = function (e) {
        isLeave = false
        let rectBounding = div.getBoundingClientRect()
        let touche = e.touches[0]
        let x = touche.clientX - rectBounding.left
        let y = touche.clientY - rectBounding.top
        div.ontouchmove = function (e) {
          let touche = e.touches[0]
          let pageX = touche.pageX
          let pageY = touche.pageY
          itemController._setStyle(div, "top", pageY - y + "px")
          itemController._setStyle(div, "left", pageX - x + "px")
          e.preventDefault()
        }
        div.ontouchend = function (e) {
          div.ontouchmove = null
          isLeave = true
          leaveBySeconds()
        }
      }
    }
    if ('ontouchstart' in document.body) {
      document.documentElement.addEventListener(
        "touchstart",
        itemControlTouchHandler,
        false
      )
      document.getElementsByClassName('ribbon')[0].style.fontSize = '16px'
    } else {
      document.documentElement.addEventListener(
        "click",
        itemControlHandler,
        false
      )
    }

window.onload = function () {
    document.body.onclick = function () {
        let nav = document.getElementById("nav");
        if(nav.style.display == "" || nav.style.display == "grid"){
            nav.style.display = "inline-grid";
            nav.style.animation = "navIn 0.5s ease-in-out  forwards"
            nav.style.position = "fixed";
        }else{
            nav.style.display = "grid";
            nav.style.animation = "navOut 0.5s ease-in-out  forwards"
        }

        let bottom = document.getElementById("bottom");
        if(bottom.style.display == "" || bottom.style.display == "grid"){
            bottom.style.display = "inline-grid";
            bottom.style.animation = "bottomIn 0.5s ease-in-out  forwards"
            bottom.style.position = "fixed";
        }else{
            bottom.style.display = "grid";
            bottom.style.animation = "bottomOut 0.5s ease-in-out  forwards"
        }
    }
}
window.addEventListener('DOMContentLoaded', function() {
    let lis = document.querySelectorAll('.wrapper li');
    let box = document.querySelector('.box');
    for (let i = 0; i < lis.length; i++) {
        let item = lis[i];
        item.onclick = function() {
            let value = this.dataset.value;
            box.style[this.dataset.prop] = value;
            let selected = document.querySelector('.current');
            if (selected) {
                selected.classList.remove('current');
            }
            
            this.classList.add('current');
        }
    }
})
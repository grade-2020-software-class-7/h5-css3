# H5-CSS3

#### 介绍
响应式布局课程相关的课件和作业

#### 软件架构
软件架构说明

#### 仓库管理
1. 请同学们fork本仓库后，然后自行在自己的远程仓库中创建以自己名字命名的文件夹。
2. 请学委帮忙把之前的课件信息和之后的课件信息及时的更新到课件的文件夹中。同学们无须在个人文件夹中再提交课件信息，避免仓库中相似的内容重复太多次。
3. 之后大家提交的作业以文件夹为单位。例如：10月10号的作业，文件夹命名：`211010-CSS作业-布局`。
4. 其他git操作和之前操作一致。

> git 指令学习：[廖雪峰的教程](https://www.liaoxuefeng.com/wiki/896043488029600)

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

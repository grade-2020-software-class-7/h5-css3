//deg为常量，表示表盘最小刻度对应的角度，值为6
const deg = 6;
window.onload = function(){
    setInterval(function(){


        var day = new Date();
        var h = day.getHours() * 30;
        var m = day.getMinutes() * deg;
        var s = day.getSeconds() * deg;

        //转换完成之后即可获取时、分、秒对应的div元素，设置其style样式
        var hr = document.getElementById('hr');
        var mi = document.getElementById('mi');
        var sc = document.getElementById('sc');

        //使用style属性设置方法完成旋转角度的设置
        var shr = h + m / 12 + 'deg';
        var smi = m + 'deg';
        var ssc = s + 'deg';
        hr.style.transform = 'rotateZ('+shr+')';
        mi.style.transform = 'rotateZ('+smi+')';
        sc.style.transform = 'rotateZ('+ssc+')';
    },300)
}
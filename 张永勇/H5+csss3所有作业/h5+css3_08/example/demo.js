window.addEventListener('DOMContentLoaded', function() {
    let inputs = document.querySelectorAll('input[name=option]');
    let box = document.querySelector('.container');
    Array.from(inputs).forEach((ele, i) => {
        ele.onchange = function() {
            let value = this.value;
            let prop = this.dataset.attr;
            box.style[prop] = value;
        }
    })
})

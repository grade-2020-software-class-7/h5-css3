window.onload = function () {
    // 获取切换按钮
    let change = document.querySelector('#change')
    let body = document.querySelector(".body")
    change.addEventListener('click', function () {
        
        if(change.textContent == "切换至inline-flex"){
            change.innerHTML = "切换至flex"
            body.style.display = "inline-flex"
        }else{
            change.innerHTML = "切换至inline-flex"
            body.style.display = "flex"
        }
    })
    let select = document.querySelectorAll("select")
        
    select.forEach(function(n){
        n.onchange = function(){
            if(this.firstElementChild.value == "flex-direction"){
                body.style.flexDirection = this.value
            }else if(this.firstElementChild.value == "flex-wrap"){
                body.style.flexWrap = this.value
            }else if(this.firstElementChild.value == "justify-content"){
                body.style.justifyContent = this.value
            }else if(this.firstElementChild.value == "align-items"){
                body.style.alignItems = this.value
            }else if(this.firstElementChild.value == "align-content"){
                this.style.alignContent = this.value
            }else if(this.firstElementChild.value == "flex-flow"){
                this.style.flexFlow = this.value
            }
        }
    })
    
    let button = document.querySelectorAll(".footb")
    button[0].onclick = function(){
        let length = body.children.length
        let newdiv = document.createElement('div')
        body.append(newdiv)
        newdiv.innerHTML = ++length
    }
    button[1].onclick = function(){
        body.removeChild(body.lastElementChild)
    }
    button[2].onclick = function(){
        let span = document.querySelector(".end")
        span.prepend("xx内联脚本xx")
    }
}
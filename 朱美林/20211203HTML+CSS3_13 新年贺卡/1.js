// 在DOM加载完后，执行我们的js
window.addEventListener('DOMContentLoaded', function() {
    let music = document.getElementById('music');
    let audio = document.querySelector('audio');
    let page1 = document.getElementById('page1');
    let page2 = document.getElementById('page2');
    let page3 = document.getElementById('page3');

    if (audio.paused) {
        music.classList.remove('play');
    } else {
        music.classList.add('play')
    }

    // 事件监听
    music.addEventListener('touchstart', function() {
        // 控制音乐播放器，播放
        if (audio.paused) {
            audio.play();
            music.classList.add('play');
            music.style.animationPlayState = 'running';
        } else {
            audio.pause();
            music.style.animationPlayState = 'paused';
        }
    });

    // 当音乐播放完后，我们暂停我们的动画。
    audio.addEventListener('ended', function() {
        music.classList.remove('play');
    });

    // 页面切换
    page1.addEventListener('touchstart', function() {
        page1.style.display = 'none';
        page2.style.display = 'block';
        page3.style.display = 'block';

        //  设置计时器，5s后，隐藏第二页
        setTimeout(function() {
            page2.classList.add('fade-out');
            page3.classList.add('fade-in')
        }, 5000);
    });
});
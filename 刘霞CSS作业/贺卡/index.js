window.addEventListener('DOMContentLoaded', function() {
    let music = document.getElementById('kai');
    let audio = document.querySelector('audio');

    let page1 = document.getElementById('one');
    let page2 = document.getElementById('two');
    let page3 = document.getElementById('three');

    if (audio.paused) {
        music.style.animationPlayState = 'paused';
    } else {
        music.style.animationPlayState = 'running';
    }

    music.addEventListener('touchstart', function() {
        // 播放音乐
        if (audio.paused) {
            audio.play();
            // 音乐停止后，删除类，再次点击的时候需要加回这个类。
            music.classList.add('play');
            music.style.animationPlayState = 'running';
        } else {
            audio.pause();
            music.style.animationPlayState = 'paused';
        }
        
    });
    // 音乐播放完后，停止动画
    audio.addEventListener('ended', function() {
        music.classList.remove('play');
    });

    // 页面切换
    page1.addEventListener('touchstart', function() {
        page1.style.display = 'none';
        page2.style.display = 'block';
        page3.style.display = 'block';

        // 设置计时器，当第二个页面动画运行结束后，直接跳转到第三个页面
        setTimeout(function() {
            page2.classList.add('fade-out');
            // page3.style.display = 'block'
            page3.classList.add('fade-in');
        }, 4000)
    })
})

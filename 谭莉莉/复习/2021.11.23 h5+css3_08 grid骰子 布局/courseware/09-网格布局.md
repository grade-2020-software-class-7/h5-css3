# Grid 网格布局

## 课前回顾

上次课我们学习了flex布局。

## 本节目的

1. 了解网格布局相关的属性，尽量多练习。
2. 属性较多，可以结合flex学过的属性一起练习。

## 1. 概述

网格布局（Grid）是目前最强大的CSS布局方案。

它将网页划分成一个个网格，可以任意组合不同的网格，做出各种各样的布局。Grid 布局与 Flex 布局有一定的相似性，都可以指定容器内部多个项目的位置。但是，它们也存在重大的区别。

- Flex 布局是轴线布局，只能指定“项目”针对轴线的位置。我们常常用来做一维布局。
- Grid 布局则是将容器划分成“行”和“列”，产生单元格，然后指定“项目”所在的单元格，可以看作是二维布局。

采用网格布局的区域，称为"容器"（container）。容器内部采用网格定位的子元素，称为"项目"（item）。

容器里面水平区域被称为 **“行”（row）**，垂直区域称为 **“列”（column）**。

行和列的交叉区域，称为 **“单元格”（cell）**。

正常情况下，`n`行和`m`列会产生`n * m`个单元格。例如：3行3列会产生9个单元格。

划分网格的线，称为 **“网格线”（grid line）**。水平网格线划分出行，垂直网格线划分出列。

正常情况下，`n`行有`n + 1`根水平网格线，`m`列有`m + 1`根垂直网格线，比如三行就有四根水平网格线。

![](images/09-grid-line.png)

### 1.1 属性

在Grid布局中，所有相关CSS属性正好分为两部分，一部分作用在grid容器上，一部分作用在grid子项上。

| 作用在grid容器上      | 作用在grid子项上  |
| --------------------- | ----------------- |
| grid-template-columns | grid-column-start |
| grid-template-rows    | grid-column-end   |
| grid-template-areas   | grid-row-start    |
| grid-template         | grid-row-end      |
| grid-column-gap       | grid-column       |
| grid-row-gap          | grid-row          |
| grid-gap              | grid-area         |
| justify-items         | justify-self      |
| align-items           | align-self        |
| place-items           | place-self        |
| jutify-content        |                   |
| align-content         |                   |
| place-content         |                   |
| grid-auto-column      |                   |
| grid-auto-rows        |                   |
| grid-auto-flow        |                   |
| grid                  |                   |

Grid 布局相关CSS属性虽然很多，但是实际上都不难理解，难的是这些属性不太容易记住，需要多多实战手写才能信手拈来。

## 2. 作用在grid容器上的属性

### 2.1 display: grid

`display: grid`指定一个容器采用网格布局。容器表现为块元素的特性。

`display: inline-grid`指定一个容器采用网格布局，容器表现为行内块元素的特性。

```css
.box1 {
  display: grid;
}
.box2 {
  display: inline-grid;
}
```

> 设为网格布局以后，容器子元素（项目）的`float`、`display: inline-block`、`display: table-cell`、`vertical-align`和`column-*`等设置都将失效。

### 2.2 grid-template-columns 和 grid-template-rows

这两个CSS属性用来为容器划分行和列。`grid-template-columns`属性定义每一列的列宽，`grid-template-row`属性定义每一列的行高。

他们属性值的语法格式一模一样。可取的语法比较复杂。我们简化一下：

```css
.container {
  grid-tempalte-columns: <track-size>... | <line-name><track-size>...;
  grid-tempalte-rows: <track-size>... | <line-name><track-size>...;
}
```

**值：**

- **`<track-size>`**：计划分的尺寸。可以是长度值，百分比值，以及`fr`单位（网格剩余空间比例单位）。
- **`<line-name>`**：网格线名称，你可以选择任意名称。

看一个简单的例子：

```css
.container {
  display: grid;
  grid-template-columns: 100px 100px 100px;
  grid-template-rows: 100px 100px 100px 100px;
}
```

`grid-tempalte-columns`后面有3个值，表示分为了3列，从左往右每列的尺寸都是`100px`。

`grid-template-rows`后面有3个值，表示分为3行，从上往下，每行的高度都是`100px`;

我们还可以给分隔线进行命名，语法是使用`[]`包裹我们自定义的名称，可以是中文：

```css
.container {
  grid-template-columns:[第一根纵线] 80px [纵线2] auto [纵线3] 100px [最后的结束线];
  grid-template-rows: [第一行开始] 25% [第一行结束] 100px [行3] auto [行4] 60px [行末];
}
```

给分割线命名是为了更好的对区域进行描述，如果我们没有描述某个区域的需求，就不需要命名了。

#### 1. repeat 语法

有的时候，我们网格的划分是很规律的。重复写同样的值比较麻烦，有其是网格很多时。这个时候，我们可以使用`repeat()`函数，简化重复的值。

```css
.container {
  display: grid;
  grid-template-columns: repeat(3, 33.33%);
  grid-template-rows: repeat(3, 33.33%)
}
```

`repeat()`接受两个参数，第一个参数是重复的次数，第二个参数是所要重复的值。

`repeat()`重复某种模式也是可以的。

```css
grid-template-columns: repeat(2, 100px 20px 80px);
```

上面的语句定义了6列，第一列和第四列的宽度为`100px`，第二列和第五列为`20px`，第三列和第六列为`80px`。

#### 2. auto-fill 关键字

有时，单元格的大小是固定的，但是容器的大小不确定。如果希望每一行（或每一列）容纳尽可能多的单元格，这时可以使用`auto-fill`关键字表示自动填充。

```css
.container {
  display: grid;
  grid-template-columns: repeat(auto-fill, 100px);
}
```

上面语句的意思是：每列宽度`100px`，然后自动填充，直到容器不能放置更多的列。

#### 3. fr 关键字

`fr`是单词fraction的缩写，表示分数。

- 简单的例子

  ```css
  .container {
    grid-template-columns: 1fr 1fr 1fr;
  }
  ```

  上面语句的意思是，网格宽度三等分。每个单元格占据宽度的三分之一。效果如下：

  ![](images/09-grid-fr-01.png)

- 如果有固定尺寸值，则划分剩余空间大小。

  ```css
  .container {
    grid-template-columns: 200px 1fr 1fr 1fr;
  }
  ```

  4列，后面3列宽度是grid容器宽度减去200px后的1/3大小。效果如下：

  ![](images/09-grid-fr-02.png)
  
- 如果和auto混用

  ```css
  .container {
      grid-template-columns: auto 1fr 1fr 1fr;
  }
  ```

  从效果看，当有设置`fr`尺寸的时候，`auto`的尺寸表现为'行内块'特性，宽度为内容的宽度。如果没有设置`fr`尺寸的网格，则表现为拉伸。

  ![](images/09-grid-fr-03.png)

- 如果`fr`数值之和小于1时

  ```css
  .container {
      grid-template-columns: auto 0.25fr 0.25fr 0.25fr;
  }
  ```

  ![](images/09-grid-fr-04.png)

  这里计算就相对复杂些，首先，由于第一个网格尺寸设置为`auto`，因此`fr`计算需要的剩余空间尺寸是grid容器的宽度减去“宽auto”这几个字符的宽度。所以，后面3个`0.25fr`元素的宽度是：`(容器宽度 - “宽auto”字符宽度) * 0.25`。然后剩余尺寸就是第一个网格宽度。

### 2.3  grid-template-areas

网格布局允许指定“区域”（area），一个区域由单个或多个单元格组成。

`grid-template-areas`属性用于定义区域。

```css
.container {
    display: grid;
    grid-template-columns: 100px 100px 100px;
    grid-template-rows: 100px 100px 100px;
    grid-template-areas: 'a b c'
        				'd e f'
        				'g h i';
}
```

上面代码先划分出9个单元格，然后将其定名为`a`到`i`的9个区域，分别对应这9个单元格。

多个单元格合并成一个区域的写法如下。

```css
grid-template-areas: 'a a a'
                     'b b b'
                     'c c c';
```

下面是一个布局实例：

```css
grid-template-areas: "header header header"
                     "main main sidebar"
                     "footer footer footer";
```

上面代码中，顶部是页眉区域`header`，底部是页脚区域`footer`，中间部分则为`main`和`sidebar`。

> **注意：**如果我们给网格区域命了名，但是没有给网格线命名，则会自动根据网格区域名称生成网格线名称，规则是区域名称后面加`-start`和`-end`。

将区域命名后，在子项目中使用'grid-area'可以指定该子项占据的区域（有点类似合并单元格）。

- 如果某些区域不需要利用，则使用"点"（`.`）表示。

  ```css
  grid-template-areas: 'a . c'
                       'd . f'
                       'g . i';
  ```

  上面代码中，中间一列为点，表示没有用到该单元格，或者该单元格不属于任何区域。

### 2.4 grid-row-gap(row-gap), grid-column-gap(column-gap), grid-gap(gap)

`grid-row-gap`属性设置行与行的间隔（行间距），`grid-column-gap`属性设置列与列的间隔（列间距）。

```css
.container {
  grid-row-gap: 20px;
  grid-column-gap: 20px;
}
```

![](images/09-grid-gap-01.png)

`grid-gap`属性是`grid-column-gap`和`grid-row-gap`的合并简写形式，语法如下。

```css
grid-gap: <grid-row-gap> <grid-column-gap>;
```

如果`grid-gap`省略了第二个值，浏览器认为第二个值等于第一个值。

> 根据最新标准，上面三个属性名的`grid-`前缀已经删除，`grid-column-gap`和`grid-row-gap`写成`column-gap`和`row-gap`，`grid-gap`写成`gap`。

### 2.5 justify-items，align-items

`justify-items`指定了网格元素的水平呈现方式，是水平拉伸显示，还是左中右对齐，语法如下：

```css
.container {
    justify-items: stretch | start | end | center;
}
```

**值**

- stretch：拉伸。表现为水平填充。
- start：表现为网格水平尺寸收缩为内容宽度，同时沿着网格线左侧对齐显示。
- end：表现为网格水平尺寸收缩为内容大小，同时沿着网格线右侧对齐显示。
- center：表现为网格水平尺寸收缩为内容大小，同时在当前网格区域内部水平居中对齐显示。

### 2.6 align-items

`align-items`指定了网格元素的垂直呈现方式，是垂直拉伸显示，还是上中下对齐，语法如下：

```css
.container {
    align-items: stretch | start | end | center;
}
```

**值**

- stretch：拉伸。表现为垂直填充。
- start：表现为网格垂直尺寸收缩为内容高度，同时沿着上网格线对齐显示。
- end：表现为网格垂直尺寸收缩为内容高度，同时沿着下网格线对齐显示。
- center：表现为网格垂直尺寸收缩为内容高度，同时在当前网格区域内部垂直居中对齐显示。

### 2.7 place-items

`place-items`可以让`align-items`和`justify-items`属性写在单个声明中。语法如下：

```css
.container {
    place-items: <align-items> <justify-items>?;
}
```

这里顺序是`align-items`在前，`justify-items`在后。

### 2.8 justify-content

`justify-content`指定了网格元素的水平分布方式。此属性仅在网格总宽度小于grid容器宽度时候有效果。

**语法**

```css
justify-content: stretch | start | end | center | space-between | space-around | space-evenly;
```

**值**

- **stretch：**默认值。拉伸，宽度填满grid容器，拉伸效果需要网格目标尺寸设为auto时候才有效，如果定死了宽度，则无法拉伸。
- **start：**与文档流方向相关。默认表现为左对齐。
- **end：**逻辑CSS属性值，与文档流方向相关。默认表现为右对齐。
- **center：**表现为居中对齐。
- **space-between：**表现为两端对齐。between是中间的意思，意思是多余的空白间距只在元素中间区域分配。
- **space-around：**around是环绕的意思，意思是每个grid子项两侧都环绕互不干扰的等宽的空白间距，最终视觉上边缘两侧的空白只有中间空白宽度一半。
- **space-evenly：**evenly是匀称、平等的意思。也就是视觉上，每个grid子项两侧空白间距完全相等。

### 2.9 align-content

`align-content`可以看成和`justify-content`是相似且对立的属性，`justify-content`指明水平方向grid子项的分布方式，而`align-content`则是指明垂直方向每一行grid元素的分布方式。如果所有grid子项只有一行，则`align-content`属性是没有任何效果的。

**语法：**

```css
align-content: stretch | start | end | center | space-between | space-around | space-evenly;
```

**值**

- **stretch：**默认值。每一行grid子元素都等比例拉伸。例如，如果共两行grid子元素，则每一行拉伸高度是50%。拉伸效果需要网格目标尺寸设为auto时候才有效，如果定死了高度，则无法拉伸。
- **start：**与文档流方向相关。默认表现为顶部堆砌。
- **end：**逻辑CSS属性值，与文档流方向相关。默认表现为底部堆放。
- **center：**表现为垂直居中对齐。
- **space-between：**表现为上下两行两端对齐。剩下每一行元素等分剩余空间。
- **space-around：**每一行元素上下都享有独立不重叠的空白空间。。
- **space-evenly：**每一行元素都完全上下等分。

### 2.10 place-content

`place-content`可以让`align-content`和`justify-content`属性写在一个CSS声明中，也就是俗称的缩写。语法如下：

```css
.container {
    place-content: <align-content> <justify-content>?;
}
```

这里顺序是`align-content`在前，`justify-content`在后。

### 2.11 grid-auto-columns, grid-auto-rows

指定任何自动生成的网格轨道（也称为隐式网格轨道）的大小。 当网格项目多于网格中的单元格或网格项目放置在显式网格之外时，将创建隐式轨道。例如：网格只有3列，但是某个项目指定在第5行。这时，浏览器会自动生成多余的网格，以便放置项目。

`grid-auto-columns`属性和`grid-auto-rows`属性用来设置，浏览器自动创建的多余网格的列宽和行高。它们的写法与`grid-template-columns`和`grid-template-rows`完全相同。如果不指定这两个属性，浏览器完全根据单元格内容的大小，决定新增网格的列宽和行高。

例如：网格划分好是`3 x 3`单元格。但是，现在我要将8号项目指定在第4行，9号项目指定在第5行。

```css
.container {
    display: grid;
    grid-template-columns: 100px 100px 100px;
    grid-template-rows: 100px 100px 100px;
    grid-auto-rows: 50px;
    grid-auto-columns: 80px;
}
.item8 {
    /* 将第8个盒子，放置在第2条纵线与第3条纵线中间 */
    grid-column: 2 / 3; 
    /* 将第8个盒子，放置在第4条行线与第5条行线中间 */
    grid-row: 4 / 5;
}
.item9 {
    /* 将第9个盒子，放置在第3条纵线与第4条纵线中间 */
    grid-column: 3 / 4;
    /* 将第8个盒子，放置在第5条行线与第6条行线中间 */
    grid-row: 5 / 6;
}
```

```html
<div class="container">
    <div class="item">1</div>
    <div class="item">2</div>
    <div class="item">3</div>
    <div class="item">4</div>
    <div class="item">5</div>
    <div class="item">6</div>
    <div class="item">7</div>
    <div class="item item8">8</div>
    <div class="item item9">9</div>
</div>
```

![](images/09-grid-auto-01.png)

### 2.12 grid-auto-flow

`grid-auto-flow`属性控制没有明确指定位置的grid子项的放置方式。比方说定义了一个5*2的10格子，共有5个元素，其中2个元素指定了放在哪个格子里，还有3个则自生自灭排列。此时，这3个元素如何排列就是由`grid-auto-flow`属性控制的。

**语法**

```css
.container {
  grid-auto-flow: row | column | row dense | column dense
}
```

**值**

- **row：**默认值，没有制定位置的网格依次水平排列优先。
- **column：**没有指定位置的网格依次垂直排列优先。
- **dense：**dense这个英文是稠密的意思。如果有设置，则表示自动排列启用“密集”打包算法。如果稍后出现的网格比较小，则尝试看看前面有没有合适的地方放置，使网格尽可能稠密紧凑。此属性值仅仅改变视觉顺序，会导致DOM属性和实际呈现顺序不符合，这对于可访问性是不友好的，建议谨慎使用。

### 2.13 grid-template, grid

`grid-template`属性是`grid-template-columns`、`grid-template-rows`和`grid-template-areas`这三个属性的合并简写形式。

`grid`属性是`grid-template-rows`、`grid-template-columns`、`grid-template-areas`、 `grid-auto-rows`、`grid-auto-columns`、`grid-auto-flow`这六个属性的合并简写形式。

从易读易写的角度考虑，还是建议不要合并属性，所以这里就不详细介绍这两个属性了。

## 3. 作用在Grid子项上的属性

### 3.1 grid-column-start，grid-column-end, grid-row-start, grid-row-end

项目的位置是可以指定的，具体方法就是指定项目的四个边框，分别定位在哪根网格线。

- `grid-column-start`属性：左边框所在的垂直网格线
- `grid-column-end`属性：右边框所在的垂直网格线
- `grid-row-start`属性：上边框所在的水平网格线
- `grid-row-end`属性：下边框所在的水平网格线

这个4个属性的取值有下列几种情况：

- 数字，表示第几根网格线。

  ```css
  .item1 {
      /* 左边框是第二根纵线 */
      grid-column-start: 2; 
      /* 右边框是第4根纵线 */
      grid-column-end: 4; 
      /* 上边框是第二根行线 */
      grid-row-start: 2;
      /* 下边框是第二根行线 */
      grid-row-end: 4;
  }
  ```

  除了1号项目以外，其他项目都没有指定位置，由浏览器自动布局，这时它们的位置由容器的`grid-auto-flow`属性决定，这个属性的默认值是`row`，因此会"先行后列"进行排列。

- 这4个属性值，除了表示第几个网格先外，还可以是指定的网格线的名称。

  ```css
  .item1 {
    grid-column-start: 第一根纵线;
    grid-column-end: 纵线2;
  }
  ```

- 这4个属性值还可以使用`span`关键字，表示“跨越”。表示跨越多少个网格。

  ```css
  .item1 {
      /* 1号项目的左边框距离右边框跨越2个网格。 */
      grid-column-start: span 2;
      /* 下面语句等效 */
      grid-column-end: span 2;
  }
  ```

### 3.2 grid-column, grid-row

`grid-column`属性是`grid-column-start`和`grid-column-end`的合并简写形式，`grid-row`属性是`grid-row-start`属性和`grid-row-end`的合并简写形式。

```css
.item {
  grid-column: <start-line> / <end-line>;
  grid-row: <start-line> / <end-line>;
}
```

下面是一个例子。

```css
.item1 {
  grid-column: 1 / 3;
  grid-row: 1 / 2;
}
/* 等同于 */
.item1 {
  grid-column-start: 1;
  grid-column-end: 3;
  grid-row-start: 1;
  grid-row-end: 2;
}
```

这两个属性之中，也可以使用`span`关键字，表示跨越多少个网格。

```css
.item1 {
  grid-column: 1 / 3;
  grid-row: 1 / 3;
}
/* 等同于 */
.item1 {
  grid-column: 1 / span 2;
  grid-row: 1 / span 2;
}
```

斜杠以及后面的部分可以省略，默认跨越一个网格。值表示从那个网格开始，默认跨越一个网格。

```css
.item1 {
  grid-column: 1;
  grid-row: 1;
}
```

### 3.3 grid-area

`grid-area`属性指定项目放在哪一个区域。

```css
.item1 {
    grid-area: e;
}
```

`e`是指在父盒子上`grid-template-area`定义的区域名。

`grid-area`属性还可用作`grid-row-start`、`grid-column-start`、`grid-row-end`、`grid-column-end`的合并简写形式，直接指定项目的位置。

```css
.item {
  grid-area: <row-start> / <column-start> / <row-end> / <column-end>;
}
```

例如：

```css
.item-1 {
  grid-area: 1 / 1 / 3 / 3;
}
```

### 3.4 justify-selft, align-selft, place-selft

`justify-self`属性设置单元格内容的水平位置（左中右），跟`justify-items`属性的用法完全一致，但只作用于单个项目。

`align-self`属性设置单元格内容的垂直位置（上中下），跟`align-items`属性的用法完全一致，也是只作用于单个项目。

```css
.item {
  justify-self: start | end | center | stretch;
  align-self: start | end | center | stretch;
}
```

这两个属性都可以取下面四个值。

- start：对齐单元格的起始边缘。
- end：对齐单元格的结束边缘。
- center：单元格内部居中。
- stretch：拉伸，占满单元格的整个宽度（默认值）。

`place-self`属性是`align-self`属性和`justify-self`属性的合并简写形式。

```css
place-self: <align-self> <justify-self>;
```

如果省略第二个值，`place-self`属性会认为这两个值相等。

## 作业

1. 用grid布局，实现上节课骰子的界面效果。
2. ![](images/09-homework.png)


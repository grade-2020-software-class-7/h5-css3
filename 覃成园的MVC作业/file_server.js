// 1.引入模块
let fs = require('fs');
let url = require('url');
let path = require('path');
let http = require('http');


//获取根目录的地址
let rootdir = path.resolve('.');

//3.创建服务
let server = http.createServer(function (req, res) {
    //先知道客户端请求的路径req.url
    //url.parse先将request获取到的url，解析一下，主要为了解码url编码，避免路径不清晰
    let filepath = path.join(rootdir, url.parse(req.url).pathname);

    //通过文件模块，判断当前文件地址状态
    fs.stat(filepath, function (err, stats) {
        if (err) {
            //读取不到，返回404
            response.writeHead(404);
            response.end('404 该文件未找到');
        } else {
            if (stats.isFile()) {
                res.writeHead(200)
                fs.createReadStream(filepath).pipe(res);
            } else if (stats.isDirectory()) {
                let filepath2 = path.join(rootdir, 'web/index.html');
                fs.createReadStream(filepath2).pipe(res);
            }
        }
    })
}).listen(3000, function () {
    console.log('服务启动，访问地址：http://127.0.0.1:3000');
})
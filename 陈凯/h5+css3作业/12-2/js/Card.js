window.onload = function () {
    let music = document.getElementById("music");
    let mu = music.firstElementChild;
    let audio = document.querySelector("audio");
    music.ontouchstart = function () {
        if (audio.paused) {
            audio.play();
        } else {
            audio.pause();
        }
        musicRun();

    }
    audio.onended = function () {
        musicRun();
    }
    function musicRun() {
        if (audio.paused) {
            mu.style.animationPlayState = "running";
        } else {
            mu.style.animationPlayState = "paused";
        }
    };
    musicRun();
    let page1 = document.getElementById("page1");
    let page2 = document.getElementById("page2");
    let page3 = document.getElementById("page3");
    page1.ontouchstart = function () {
        // page1.classList.add("anima")
        page1.style.display = "none";
        page2.style.display = "block";
        setTimeout(function(){
            page3.style.display = "block"
            page2.classList.add("anima");
            page3.classList.add("anima");
        },5000)
    }
}
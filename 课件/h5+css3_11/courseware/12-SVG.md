# SVG  可缩放矢量图形 

## 课前回顾



## 本节目

1. 简单了解一下SVG代码结构，学会绘制一些简单图形
2. 开发过程中，我们如何使用SVG图像

## 1. SVG 简介

SVG 的第一个版本是在2001年推出的。它是一种基于 XML 语法的图像格式，全称是可缩放矢量图（Scalable Vector Graphics）。  其他图像格式都是基于像素处理的，SVG 则是属于对图像的形状描述，所以它本质上是文本文件，体积较小，且不管放大多少倍都不会失真。 

> **什么是XML？**
>
> XML：可扩展标记语言（EXtensible Markup Language）。是一种很像HTML的标记语言。
>
> 它的设计宗旨是传输数据，而不是显示数据。
>
> 接下来我们学习SVG的内容，来大致了解一下XML。

### 1.1 SVG 优势

1. 可以使用任何文本编辑器来创建绘画SVG.
2. SVG图像可以扩展，可以在任何分辨率上高质量显示。
3. SVG图像支持缩放，且不会失去任何质量。
4. SVG 是开发标准，是纯XML文件

**在网页上绘制一个简单的SVG**

-  SVG 文件可以直接插入网页，成为 DOM 的一部分，然后用 JavaScript 和 CSS 进行操作。 

  ```html
  <!DOCTYPE html>
  <html>
  <head></head>
  <body>
  <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
     <circle cx="50" cy="50" r="40" stroke="black" stroke-width="2" fill="red" />
  </svg> 
  </body>
  </html>
  ```

- SVG 代码也可以写在一个独立文件中，然后用`<img>`、`<object>`、`<embed>`、`<iframe>`等标签插入网页。 

  ```html
  <img src="circle.svg">
  <object data="circle.svg" type="image/svg+xml"></object>
  ```

  > - `<object>`标签是装载非HTML内容的容器。
  > - `<embed>` 标签被主流浏览器支持，允许使用脚本。不过它不是HTML规范，不推荐使用。

- CSS 也可以使用 SVG 文件

  ```css
  .logo {
      background: url(icon.svg);
  }
  ```

## 2. 语法

在SVG元素使用的是坐标系统（网格系统），和`canvas`类似。以页面的左上角为起点，以`px`为单位。

### 2.1 `<svg>`标签

 SVG 代码都放在顶层标签`<svg>`之中。下面是一个例子。 

```html
<svg width="100%" height="100%">
  <circle id="mycircle" cx="50" cy="50" r="50" />
</svg>
```

 `<svg>`的width属性和height属性，指定了 SVG 图像在 HTML 元素中所占据的宽度和高度。除了相对单位，也可以采用绝对单位（单位：像素）。如果不指定这两个属性，SVG 图像的大小默认为300像素（宽）x 150像素（高）。

#### 2.1.1 viewBox属性

 如果只想展示 SVG 图像的一部分，就要指定`viewBox`属性。 

```html
<svg width="100" height="100" viewBox="50 50 50 50">
  <circle id="mycircle" cx="50" cy="50" r="50" />
</svg>
```

如果不看`viewBox`，SVG 尺寸是100*100像素，圆只占据其中的以小部分。而这只了viewBox后，展示的内容发生的巨大的变化。

`<viewBox>`属性的值有四个数字，分别是左上角的横坐标和纵坐标、视口的宽度和高度。

```ini
videBox="x, y, width, height" // x:左上角横坐，y：左上角纵坐标， width：宽度，height：高度
```

 我们可以将`viewBox`的效果理解为：SVG就像是我们的显示器屏幕，viewBox 截屏工具选中的那个框框，最终的呈现是把框框中的截屏内容再次在显示器中**全屏显示**。

**注意：**视口必须适配所在的空间。上面代码中，视口的大小是50 * 50，`viewBox`属性指定视口从`(50, 50)`这个点开始，实际看到的是右下角的四分之一圆。由于 SVG 图像的大小是 100 * 100，所以视口会放大去适配 SVG 图像的大小，即放大了四倍。

如果SVG不指定`width`属性和`height`属性，只指定`viewBox`属性，则相当于只给定 SVG 图像的长宽比。这时，SVG 图像的大小默认是所在的 HTML 元素的大小。

### 2.2  `<circle>`标签

`<circle>`标签代表圆形。

```xml
<svg width="300" height="180">
  <circle cx="30"  cy="50" r="25" />
  <circle cx="90"  cy="50" r="25" class="red" />
  <circle cx="150" cy="50" r="25" class="fancy" />
</svg>
```

`class`属性用来指定对应的 CSS 类。

```css
.red {
  fill: red;
}

.fancy {
  fill: none;
  stroke: black;
  stroke-width: 3pt;
}
```

SVG 的 CSS 属性与网页元素有所不同。

- fill：填充色
- stroke：描边色
- stroke-width：边框宽度

> 给SVG添加样式：
>
> - 使用外部样式表：
>
>   - 第一种，直接在写HTML CSS样式的地方加上SVG相关的样式。
>
>   - 第二种，在SVG标签内部，引入外部CSS文件。通常在`defs`标签中引入。
>
>     ```html
>     <link href="style.css" type="text/css" rel="stylesheet"/>
>     ```
>
>     或者是：
>
>     ```xml
>     <?xml-stylesheet href="style.css" type="text/css" ?>
>     ```
>
> - 使用内联样式为SVG添加样式：
>
>   - 在`defs`标签内，直接添加`style`标签来写样式
>
>     ```html
>     <defs>
>     	<style type="text/css">
>         .red {stroke: red;}
>       </style>
>     </defs>
>     ```
>
>   - 直接在标签的`style`属性里面添加样式。

### 2.3 `<line>`标签

`<line>`标签用来绘制直线。

```html
<svg width="300" height="180">
	<line x1="0" y1="0" x2="200" y2="0" style="stroke: rgb(0, 0, 0); stroke-width: 5"></line>
</svg>
```

上面代码中，`<line>`标签的`x1`属性和`y1`属性，表示线段起点的横坐标和纵坐标；`x2`属性和`y2`属性，表示线段终点的横坐标和纵坐标；`style`属性表示线段的样式。

### 2.4 `<polyline>`标签

`<polyline>`标签用于绘制一根折线。

```xml
<svg width="300" height="180">
  <polyline points="3,3 30,28 3,53" fill="none" stroke="black" />
</svg>
```

`<polyline>`的`points`属性指定了每个端点的坐标，横坐标与纵坐标之间与逗号分隔，点与点之间用空格分隔。

### 2.5 `<rect>`标签

`<rect>`标签用于绘制矩形。

```xml
<svg width="300" height="180">
	<rect x="0" y="0" height="100" width="200" style="stroke: black; fill: silver"></rect>
</svg>
```

`<rect>`的`x`属性和`y`属性，指定了矩形左上角端点的横坐标和纵坐标；`width`属性和`height`属性指定了矩形的宽度和高度（单位像素）。

### 2.6 `<ellipse>`标签

`<ellipse>`标签用于绘制椭圆。

```xml
<svg width="300" height="180">
  <ellipse cx="60" cy="60" ry="40" rx="20" stroke="black" stroke-width="5" fill="silver"/>
</svg>
```

`<ellipse>`的`cx`属性和`cy`属性，指定了椭圆中心的横坐标和纵坐标（单位像素）；`rx`属性和`ry`属性，指定了椭圆横向轴和纵向轴的半径（单位像素）。

### 2.7 `<polygon>`标签

`<polygon>`标签用于绘制多边形。

```xml
<svg width="300" height="180">
  <polygon fill="green" stroke="orange" stroke-width="1" points="0,0 100,0 100,100 0,100 0,0"/>
</svg>
```

`<polygon>`的`points`属性指定了每个端点的坐标，横坐标与纵坐标之间与逗号分隔，点与点之间用空格分隔。

### 2.8 `<path>`标签

`<path>`标签用于绘制路径。

```xml
<svg width="300" height="180">
<path d="
  M 18,3
  L 46,3
  L 46,40
  L 61,40
  L 32,68
  L 3,40
  L 18,40
  Z
"></path>
</svg>
```

`<path>`的`d`属性表示绘制顺序，它的值是一个长字符串，每个字母表示一个绘制动作，后面跟着坐标。

- M：移动到（moveto）
- L：画直线到（lineto）
- Z：闭合路径

### 2.9 `<text>`标签

`<text>`标签用于绘制文本。

```xml
<svg width="300" height="180">
  <text x="50" y="25">Hello World</text>
</svg>
```

`<text>`的`x`属性和`y`属性，表示文本区块基线（baseline）起点的横坐标和纵坐标。文字的样式可以用`class`或`style`属性指定。

### 2.10 `<use>`标签

`<use>`标签用于复制一个形状。

```xml
<svg viewBox="0 0 30 10" xmlns="http://www.w3.org/2000/svg">
  <circle id="myCircle" cx="5" cy="5" r="4"/>

  <use href="#myCircle" x="10" y="0" fill="blue" />
  <use href="#myCircle" x="20" y="0" fill="white" stroke="blue" />
</svg>
```

`<use>`的`href`属性指定所要复制的节点，`x`属性和`y`属性是`<use>`左上角的坐标。另外，还可以指定`width`和`height`坐标。

### 2.11 `<g>`标签

`<g>`标签用于将多个形状组成一个组（group），方便复用。

```xml
<svg width="300" height="100">
  <g id="myCircle">
    <text x="25" y="20">圆形</text>
    <circle cx="50" cy="50" r="20"/>
  </g>

  <use href="#myCircle" x="100" y="0" fill="blue" />
  <use href="#myCircle" x="200" y="0" fill="white" stroke="blue" />
</svg>
```

### 2.12 `<defs>`标签

`<defs>`标签用于自定义形状，它内部的代码不会显示，仅供引用。

```xml
<svg width="300" height="100">
  <defs>
    <g id="myCircle">
      <text x="25" y="20">圆形</text>
      <circle cx="50" cy="50" r="20"/>
    </g>
  </defs>

  <use href="#myCircle" x="0" y="0" />
  <use href="#myCircle" x="100" y="0" fill="blue" />
  <use href="#myCircle" x="200" y="0" fill="white" stroke="blue" />
</svg>
```

### 2.13 `<pattern>`标签

`<pattern>`标签用于自定义一个形状，该形状可以被引用来平铺一个区域。

```xml
<svg width="500" height="500">
  <defs>
    <pattern id="dots" x="0" y="0" width="100" height="100" patternUnits="userSpaceOnUse">
      <circle fill="#bee9e8" cx="50" cy="50" r="35" />
    </pattern>
  </defs>
  <rect x="0" y="0" width="100%" height="100%" fill="url(#dots)" />
</svg>
```

上面代码中，`<pattern>`标签将一个圆形定义为`dots`模式。`patternUnits="userSpaceOnUse"`表示`<pattern>`的宽度和长度是实际的像素值。然后，指定这个模式去填充下面的矩形。

### 2.14 `<image>`标签

`<image>`标签用于插入图片文件。

```xml
<svg viewBox="0 0 100 100" width="200" height="200">
  <image xlink:href="path/to/image.jpg"
    width="50%" height="50%"/>
</svg>
```

上面代码中，`<image>`的`xlink:href`属性表示图像的来源。

### 2.15 `<animate>`标签

`<animate>`标签用于产生动画效果。

```xml
<svg width="500px" height="500px">
  <rect x="0" y="0" width="100" height="100" fill="#feac5e">
    <animate attributeName="x" from="0" to="500" dur="2s" repeatCount="indefinite" />
  </rect>
</svg>
```

上面代码中，矩形会不断移动，产生动画效果。

`<animate>`的属性含义如下。

> - attributeName：发生动画效果的属性名。
> - from：单次动画的初始值。
> - to：单次动画的结束值。
> - dur：单次动画的持续时间。
> - repeatCount：动画的循环模式。

可以在多个属性上面定义动画。

```xml
<animate attributeName="x" from="0" to="500" dur="2s" repeatCount="indefinite" />
<animate attributeName="width" to="500" dur="2s" repeatCount="indefinite" />
```

### 2.16 `<animateTransform>`标签

`<animate>`标签对 CSS 的`transform`属性不起作用，如果需要变形，就要使用`<animateTransform>`标签。

```xml
<svg width="300px" height="500px">
  <rect x="150" y="150" width="50" height="50" fill="#4bc0c8">
    <animateTransform attributeName="transform" type="rotate" begin="0s" dur="10s" from="0 200 200" to="360 300 300" repeatCount="indefinite" />
  </rect>
</svg>
```

上面代码中，`<animateTransform>`的效果为旋转（`rotate`），这时`from`和`to`属性值有三个数字，第一个数字是角度值，第二个值和第三个值是旋转中心的坐标。`from="0 200 200"`表示开始时，角度为0，围绕`(200, 200)`开始旋转；`to="360 400 400"`表示结束时，角度为360，围绕`(400, 400)`旋转。

## 3. 使用

在本节第一部分，我们介绍了SVG在web页面上可以如何使用。在大部分的业务使用场景中，我们是将SVG当做静态背景图片或者图标使用，我们只需将svg图片引入到我们的页面中即可。

当前市面上有很多GUI（图形用户界面）编辑器，帮助我们绘制SVG图像（这个工作常常由UI来实现）。如果说你当前没有UI给我们绘制这些图像，我们去图标服务网站无下载免费svg文件。

### 3.1 iconfont介绍

[使用介绍](https://www.iconfont.cn/help/detail?spm=a313x.7781069.1998910419.d8d11a391&helptype=code)

1. 我们搜索我们需要的图片，然后可以直接下载也可以先加入购物车后，在批量下载。
2. 当前我们下载了SVG文件。
3. 在HTML文件中引入SVG图像。

```html
<body>
  <object data="svg/close.svg" tyle="image/svg+xml"></object>
</body>
```

不过这种写法有个弊端，就是你无法直接使用外部css来控制样式，如果要使用外部css，必须是在SVG标签内引入外部css文件。这样子如果我们想用外部css样式时，需要修改SVG文件的内容。这样子非常不便利，且如果这个svg图像在另外一个页面使用时，不需要使用外部样式时，会造成流量浪费，下载多余的样式文件。

### 3.2 SVG sprite技术

这里的Sprite技术，类似于CSS中的Sprite技术。“雪碧图”，将小图标整合在一起，实际呈现的时候准确显示特定图标。

SVG Sprite技术，目前最佳实践是使用`symbol`元素。`symbol`元素是什么？单纯翻译的话，是“符号”的意思。这里我们可以将它当做是“元件”。

我们把SVG元素看做是一个画布，而`symbol`则是画布上一个一个组装好的元件，这写元件就是我们即将使用的svg图标。

```xml
<svg>
    <symbol>
        <!-- 第1个图标路径形状之类代码 -->
    </symbol>
    <symbol>
        <!-- 第2个图标路径形状之类代码 -->
    </symbol>
    <symbol>
        <!-- 第3个图标路径形状之类代码 -->
    </symbol>
</svg>
```

上面代码，我们将图标元件生成好，之后该如何在使用的？这就需要使用SVG中的`<use>`元素。

#### 3.2.1 use 元素

use元素是SVG中非常强大，非常重要的一个元素。

- 它可以重复调用一个定义好的图像
- 可以跨SVG调用

##### 3.2.1.1 可重复调用

```html
<svg>
  <defs>
    <g id="shape">
        <rect x="0" y="0" width="50" height="50" />
        <circle cx="0" cy="0" r="50" />
    </g>
  </defs>

  <use xlink:href="#shape" x="50" y="50" />
  <use xlink:href="#shape" x="200" y="50" />
</svg>
```

user元素四通过`xlink:href`属性，寻找要使用的元素的。`#shape`对应的就是`id`为`shape`的元素。`use`元素可以有自己的坐标，以及支持`transform`变换，甚至可以use其他`use`元素。

##### 3.2.1.2 跨SVG调用

SVG中的`use`元素可以调用其他SVG文件的元素，只要在一个文档中。

```html
<!-- 在上面的例子下，添加下面代码 -->
<svg width="500" height="110">
	<use xlink:href="#shape" x="50" y="50"></use>
</svg>
```

而这个**跨SVG调用**就是“SVG Sprite技术”的核心所在。

我们只要在页面某处载入一个充满Sprite(symbol)的SVG文件，于是，在页面的任何角落，只要想使用这个图标，只要简单一点代码就可以了：

```html
<svg class="size"><use xlink:href="#target"></use></svg>
```

图标尺寸，我们可以通过CSS控制，内容只有一个use元素。代码简介，元素样式也容易控制，容易维护。

#### 3.2.2 如何将SVG图标变成symbol并整合在一起？

我们从iconfont网址上下了我们需要的svg图片，接下来，如何将这个图片整合在一起呢？

给大家推荐一个工具：[svg压缩合并工具](https://www.zhangxinxu.com/sp/svgo/)

下学期我们学习VUE.JS时，使用webpack打包技术，使用插件，可以将我们指定文件夹内的svg文件，全部整合成一个SVG Sprite文件。

## 作业

1. 上次课作业，小说阅读页面，里面的图标，使用svg方式来设置。

2. 尝试画出下面的图片

   ![](images/12-homework.png)

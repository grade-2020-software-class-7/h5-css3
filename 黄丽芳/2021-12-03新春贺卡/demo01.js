window.addEventListener('DOMContentLoaded', function () {
    let page1 = document.getElementById('page1')
    let page2 = document.getElementById('page2')
    let page3 = document.getElementById('page3')
    page1.addEventListener("touchstart", function () {
        page1.style.display = 'none'
        page2.style.display = 'block'

        setTimeout(function () {
            page2.classList.add('fade-out');
            page3.classList.add('fade-in')
            page3.style.display = 'block'
            page3.style.top = "100%"
        }, 6000)

    })


})
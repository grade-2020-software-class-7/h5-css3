# 案例分析——样式

## 1. 全局样式

- 我们先使用通配符选择器，将浏览器默认添加的内外边距的值清除。

  ```css
  * {
      margin: 0;
      padding: 0;
  }
  ```

- 因为是响应式的页面，页面中的字体还有图片大小会随着设置尺寸不一样而有变化。所以在长度单位上的选择，我们使用`rem`，`vw`，`vh`来设置。

- 根据设计稿（暂时找不到素材），我们可以计算出，字体大小和设计稿宽度的比例：1.526vw。

  ```css
  html {
      font-size: 1.526vw;
      font-family: 'Microsoft YaHei';
  }
  ```

- 页面高度。我们发现，我们的body的高度就是我们内容的高度。当我们想要让body撑满我们的可视窗口的时候。我们需要将它的高度设置为`100%`。

  ```css
  body {
      height: 100%;
  }
  ```

  只是设置这个效果的时候，会发现body的高度仍旧是内容的高度。这是因为，在HTML5页面中，html，body等标签，被当作了块元素处理。即宽度撑满，高度由内容填充。body设置高度`100%`时，其实是获取HTML高度的`100%`。但因为HTML的高度也是由内容决定的，所以单独设置没有效果。

  应该给html和body都设置高度100%；html高度设置100%，继承了浏览器的高度。

  ```css
  html,
  body {
      height: 100%;
  }
  ```

## 2. 音乐播放器

查看音乐播放器，查看素材，我们可以看到音乐播放器只有两张图片。

![](images/14/14-music-01.png)

而我们的效果图如下：

![](images/14/14-music-02.png)

具体分析：

- 从定位分析：我们的这个音乐盒子一直固定在页面的右上角。所以我们使用绝对定位。左右边距：

  ```css
  #music {
      position: fixed;
      top: 3vh;
      right: 4vw;
  }
  ```

- 根据设计稿，我们这个音乐盒子的大小：

  > 宽：15vw；
  >
  > 高：15vw；
  >
  > 圆角：50%；
  >
  > 边框：宽度4px，颜色#ef1635，样式：实线；
  >
  > 背景色：白色

- **指针**：

  作为盒子的第一个子元素，我们的css选择器使用`#music > img:first-of-type `。

  指针是贴着边框展示的。所以我们用绝对定位要布局。

  ```css
  #music > img:first-of-type {
      position: absolute;
      top: 24%;
      right: 2.5%;
      width: 28.426%;
  }
  ```

  > 这些定位信息，是通过设计稿计算出来的。

- **磁盘：**

  磁盘我们可以看到在盒子的正中间。大小通过设计稿的值计算，可以得出`width：79%;`

  要让我们的磁盘垂直居中，有如下三种方法：

  - flex布局：直接写在`#music`元素上

    ```css
    #music {
        display: flex;
         /* 控制元素水平居中 */
        justify-content: center;
        /* 控制元素垂直居中 */
        align-items: center;
    }
    ```

  - absolute + transform

    磁盘元素作为最后一个子元素

    ```css
    #music > img:last-of-type {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    ```

    因为后续我们要将磁盘转动起来，我们需要使用`transform: rotate()`属性。这个时候，`transform`属性的状态值由`translate()`转变为`rotate()`。产生的效果不是我们预期的。

    **所以：这种方式在这里不适用。**

  - absolute + margin: auto

    这是一个新的垂直居中的写法。即将`top`, `right`, `bottom`, `left`的值都设置了，然后`margin: auto`也可以设置水平垂直居中。

    ```css
    #music > img:last-of-type {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        margin: auto;
    }
    ```

    > 这个方式，是我的例子里使用的方法。


## 3. 第一页

第一页，我们先设置页面的大小。因为我们的内容分三屏展示，所以我们定义的每一页大小都应该撑满我们的可视窗口。

```css
.page {
  width: 100%;
  height: 100%;
}
```

- 背景图

  我们给每个页面里面添加了一个div来放置背景图片，为了不让背景图片影像我页面上的其他元素，我们将背景图片设置为绝对定位。对此，我们要将page先设置为相对定位。

  ```css
  .page {
    position: relative;
  }
  .bg {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
  ```

  然后针对每个页面，使用自己独立的背景图。

  ```css
  #page1 > .bg{
  	background: url(./images/p1_bg.jpg) no-repeat center center;
    background-size: 100%;
  }
  ```

  > 对背景图片的设置，我们可以拆分写属性，并将这些属性都一起设置在.bg选择器里面。
  >
  > ```css
  > .bg {
  >   background-repeat: no-repeat;
  >   background-position: center center;
  >   background-size; 100%;
  > }
  > #page1 > .bg {
  >   background-image: url(./images/p1_bg.jpg);
  > }
  > ```

- **页面中灯笼**

  灯笼，我们在结构分析时，我们已经预设要用背景图片来加载这个图片。经过测量我们发现灯笼图片的高度中点位置并不是在灯笼重点圆圈的中心位置，为了不影响我们到时文本居中效果。所以为了之后我们处理图片和闪烁的红圈，我们扩大图片的大小为：`width: 45vw; height: 71.2vh;`，然后将背景图片往底边对齐。

  ```css
  #page1 > .p1-denglong {
    width: 45vw;
    height: 71.2vh;
    /* 让图片水平居中 */
    margin: 0 auto;
    background: url(./images/p1_lantern.png) no-repeat center bottom;
    background-size: 100%;
  }
  ```

  这样子设置后，我们发现图片距离顶部有一段位置，并不是平整的接触顶部。这时候我们要将图形向上移动一段位置，通过测量具体是`3.1vh`的高度。对于平移，我们可以使用下列两种方法。

  - 相对定位偏移

    ```css
    #page1 > .p1-denglong {
      /*...*/
      position: relative;
      top: -3.1vh;
    }
    ```

  - 决对定位法

    ```css
    #page1 > .p1_denglong {
      /* ... */
      position: absolute;
      top: -3.1vh;
      left: 0;
      right: 0;
      margin: 0 auto;
    }
    ```

- 设置灯笼中间的文本

  将灯笼往上移后，文本也跟着上移被遮挡了。我们需要将文字调整到我们盒子的正中心。我们可以用flex布局，让文本可以在处置方向上处置居中，也可以通过测量，直接将文本距离上边距的高度测量出来。

  ```css
  #page1 > .p1_denglong {
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
    /* 计算得来的值 */
    font-size: 3.506rem; 
  }
  ```

  or

  ```css
  #page1 > .p1_denglong {
    padding-top: 31vh;
    text-align: center;
  }
  ```

  上面代码，设置内边后，我们灯笼盒子的高度发生了变化。这不是我们想要的。这个时候，我们可以增加一个属性`box-sizing: border-box`。

  > **box-sizing**
  >
  > - 默认值为：content-box。这种情况下，我们给盒子设置的宽度和高度是盒子模型中`content`区域的宽度和高度。而我们的盒子的大小 = content + padding + border。所以我们设置了padding后，盒子高度变了，背景图片位置错乱。
  > - border-box：当设置为这个值后，我的盒子大小就是我们设置的width和height。增加了padding值之后，会压缩我们content区域的大小。

- 闪烁的光圈设置

  因为这个光圈是没有实际意义的，我们为了简化我们的结构。我们用CSS伪元素来绘制这个光圈。光圈一样是垂直水平居中的。我们从之前的方法中选择一种来实现。

  ```css
  #page1 > .p1-denglong::before {
    content: "";
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    margin: margin;
    width: 30vw;
    height: 30vw;
    background-color: #d60b3b;
    opacity: 0.5;
    box-shadow: 0 0 10vw 10vw #d60b3b;
    border-radius: 50%;
  }
  ```

  > 动画效果，我们后面统一来加。

- 接下来是页面底部的logo和文字展示。我们均使用绝对定位来布局。

  ```css
  #page1 > .p1-logo {
    position: absolute;
    left: 0;
    right: 0;
    bottom: 9vh;
    background: url(./images/p1_minxi.png) no-repeat center center;
    background-size: 100%;
    width: 50vw;
    height: 10vh;
    margin: auto;
  }
  #page1 > .p1-words {
    font-size: 2.134rem;
    position: absolute;
    left: 0;
    right: 0;
    bottom: 48px;
    text-align: center;
  }
  ```

## 4. 第二页

第二页样式比较简单。一个背景图片，中间有三个圈，一个年份的图片。

- 背景图，和页面一一样的处理，只是背景图片不一样。

  ```css
  #page2 > .bg {
    background-image: url(./images/p2_bg.jpg);
  }
  ```

- 中间的文字图片设置，垂直居中。

  ```css
  #page2 > .p2-year {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    margin: auto;
    background: url(./images/p2_2022.png) no-repeat center center;
    background-size: 100%;
    width: 35vw;
    height: 35vw;
  }
  ```

- 中间的圆圈，我们要放置三个圆。所以我们分别一个用在背景图，一个用在`::before`伪元素，一个用在`::after`伪元素。

  > 最外面的圆的大小为：60vw * 60vw
  >
  > 中间的圆的大小为：46vw * 46vw
  >
  > 最内侧的圆大小为：40vw * 40vw

## 5. 第三页

第三页的内容，分为背景，logo，对联，贺词，福字。

代码略。

位置信息：

- logo：大小：50vw * 6vh， top: 8vh
- 贺词：大小：48vw * 50vh, top: 21vh
- 左边对联：大小：23vw * 42vh, top: 25.5vh, left: 3.75vw
- 右边对联：大小：23vw * 42vh, top: 25.5vh, right: 3.75vw
- 福：大小：35vw * 35vw, bottom: 5vh

## 6. 页面动画效果

### 6.1 音乐播放器

磁盘的旋转：磁盘可以一直旋转下去，所以我们使用animation来设置动画。转一圈时间 4s，匀速转动。因为我们的播放器不是一直旋转的，它是可以暂停的，所以我么增加一个类来专门处理动画。

```css
#music > img.play {
  animation: music_disc 4s linear infinite;
}
@keyframes music_disc {
  from {transform: rotate(0deg);}
  to {transform: rotate(360deg);}
}
```

### 6.2 第一页

中心圆圈的闪烁，我们可以看出，它其实在重复的做放大、缩小的效果。且这个变化是来来回回的。所以我们需要设置animation的另外一个属性，`animation-direction:alternate` 动画先执行一遍，然后再反向执行一遍。

```css
#page1 > .p1-denglong::before  {
  animation: p1_lantern 0.5s infinite alternate；
}
@keyframe p1_lantern {
  from {
    opacity：0.5；
    transfrom: scale(0.8, 0.8);
  }
  to {
    opacity: 1;
  }
}
```

### 6.3 第二页

中间的三个圈转动，分别给三个圈设置动画。

- 最里面的圈是最开始动的。而且转速比较快，我们设置动画时间为1s，匀速转动，延迟时间为1s（一开始进入页面的时候没有转动），转动次数不限制。

  每次逆时针转动3圈。

- 中间的圈，是在里面的圈转后才之执行转动的。且转动的方向不一样。

  动画时长也是1s，延迟2s开始转动（等内圈执行一次动画后再执行），匀速转动，不限次数。

  每次动画都顺时针转动2圈。

- 最外层的圈，转动最缓慢，延时时间最长。

  动画时长也为1s，延迟时间3s，匀速运动，不限次数。

  每次动画逆时针转1圈。

### 6.4 第三页

这个页面只有一个匀速转动的福字。

动画过渡时间2s，匀速转动，不限次数。每次都顺时针旋转1圈。


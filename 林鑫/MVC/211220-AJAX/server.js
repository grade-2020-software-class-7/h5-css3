let http = require('http')
let url = require('url')
let path = require('path')
let fs = require('fs')

//创建服务器
http.createServer(function (req, res) {
    let filepath=path.join(path.resolve('.'),url.parse(req.url).pathname)
    fs.stat(filepath,function(err,stats){
        if(err){
            res.writeHead('500')
            res.end();
        }else{
            if(stats.isFile()){
                res.writeHead('200')
                fs.createReadStream(filepath).pipe(res)
            }else {
                let obj = {
                    name:'DAYTOY'
                }
                res.writeHead('200')
                res.write(JSON.stringify(obj))
                res.end()
            }
        }
    })
})
.listen(3000, function() {
    console.log('server start on http://127.0.0.1:3000');
})
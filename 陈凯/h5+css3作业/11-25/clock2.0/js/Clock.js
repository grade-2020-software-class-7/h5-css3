window.onload = function () {
    //创建小时刻度
    let scaleHour = document.getElementById("scale-hour");
    for (let i = 0; i < 13; i++) {
        let li = document.createElement("li");
        if (i > 0) {
            li.innerHTML = "<span>" + i + "</span>"
        }
        li.style.transform = "rotate(" + i * 30 + "deg)"
        scaleHour.append(li);
    }
    //创建分钟刻度
    let scaleMinute = document.getElementById("scale-minute");
    for(let ii = 0;ii < 61;ii++){
        let Li = document.createElement("li");
        Li.style.transform = "rotate("+ ii * 6 +"deg)"
        scaleMinute.append(Li);
    }
}
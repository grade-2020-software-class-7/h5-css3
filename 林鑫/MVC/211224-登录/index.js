// 1. 引入模块
const express = require('express');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const md5= require('md5')
const app = express();

// 引入数据库驱动
const mysql = require('mysql');

// 数据库连接配置
const db = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '123456',
    database : 'study'
});

db.connect();

// 使用中间件，帮我处理post请求的body数据
app.use(express.urlencoded()); // 处理的请求数据类型是： 'application/x-www-form-urlencoded'
app.use(cookieParser());
app.use(session({
    secret: '123456', // 建议使用 128 个字符的随机字符串
    cookie: { maxAge: 60 * 1000 * 3 }
}))

// 生成一个web静态页面服务器
app.use('/web', express.static(__dirname + '/web'));

// 自己造点假数据
// let users = {
//     admin: 'root123',
//     root: '123456',
//     test: 'abc123'
// };

app.use(function(req, res, next) {
    console.log('cookie = ', req.cookies);
    // 获取session的信息
    console.log('session = ', req.session);
    next();
});

// 写接口响应的逻辑
app.post('/signin', function(req, res) {
    // 获取到请求的参数
    let data = req.body; // {username: '', password: ''}

    // 数据库查询是否存在这个用户名
    let sql = `SELECT * FROM user_table WHERE username='${data.username}'`;
    db.query(sql, function(err, result) {
        if (err) {
            res.send({code: 'E002', msg: '数据库查询报错！'});
        } else {
            // 判断是否有结果
            if (result.length > 0) {
                // 说明存在用户名, 接下来判断密码是否正确
                if (data.password === result[0].password) {
                    // 存储session的信息
                    req.session.user = result[0].ID;
                    res.send({code: '200', msg: '', data: 'success'});
                } else {
                    res.send({code: 'E001', msg: '密码错误！'});
                }
            } else {
                res.send({code: 'E001', msg: '用户不存在！'});
            }
        }
    });

    db.end();
});

// 写一个注册响应接口
app.post('/register', function(req, res) {
    let body = req.body;
    // 数据插入功能
    let sql = `INSERT INTO user_table(username, password) VALUES ('${body.username}', '${md5(body.password)}')`;
    db.query(sql, function(err, result) {
        if (err) {
            console.log(err);
            res.send({code: 'E002', msg: '数据库插入报错！'});
        } else {
            // 存储session的信息
            console.log(result);
            req.session.user = result.insertId;
            res.send({code: '200', msg: '', data: 'success'});
        }
    });
    db.end();
})
app.listen(8080, function() {
    console.log('server running on http://127.0.0.1:8080');
});
window.onload = function () {
    let input = document.querySelectorAll('input')
    let submit = input[5]
    let spanerror = document.querySelectorAll('.span-error')
    console.log(spanerror);
    let vipname = input[0];
    let password = input[1];
    let repassword = input[2]
    let phone = input[3]
    submit.addEventListener('click', function () {
        for (let j = 0; j < input.length; j++) {
            if (j == 0) {
                vipnameNull();
            }
            if (j == 1) {
                passwordNull();
            }
            if (j == 2) {
                repasswordNull();
            }
            if (j == 3) {
                phoneNull();
            }
        }
    })
    function vipnameNull() {
        let errors = spanerror[0]
        if (vipname.value == '') {
            vipname.style.border = '1px solid red'
            errors.style.display = 'block'
            errors.textContent = '请设置会员名';
            errors.style.color = 'red'

        } else {
            errors.style.display = 'none'
            // vipname.style.borderColor = '#e1e1e1'
        }
    }
    function passwordNull() {
        let errors1 = spanerror[1]
        if (password.value == '') {
            password.style.border = '1px solid red'
            errors1.style.display = 'block'
            errors1.textContent = '请设置登录密码';
            errors1.style.color = 'red'
        } else {
            errors1.style.display = 'none'
            // password.style.border = '1px solid #e1e1e1'
        }
    }
    function repasswordNull() {
        let errors2 = spanerror[2]
        if (repassword.value == '') {
            repassword.style.border = '1px solid red'
            errors2.style.display = 'block'
            errors2.textContent = '请再次确认登录密码';
            errors2.style.color = 'red'
        } else {
            errors2.style.display = 'none'
            // repassword.style.border = '1px solid #e1e1e1'
        }
    }
    function phoneNull() {
        let errors5 = spanerror[3];
        if (phone.value == '') {
            phone.style.border = '1px solid red'
            errors5.style.display = 'block'
            errors5.textContent = '请输入手机号';
            errors5.style.color = 'red'
        } else {
            errors5.style.display = 'none'
            // phone.style.border = '1px solid #e1e1e1'
        }
    }
    input.forEach(function (value, index) {
        value.oninput = function () {
            if (index == 0) {
                vipnamecheck();
            }
            if (index == 1) {
                passwordcheck();
            }
            if (index == 2) {
                repasswordcheck()
            }
            if (index == 3) {
                phonecheck()
            }
        }
    })
    function vipnamecheck() {
        let reg = /^(?!(\d+)$)[\u4e00-\u9fffa-zA-Z\d\-_]+$/;
        let errors = spanerror[0]
        if (reg.test(vipname.value)) {
            errors.style.display = 'none'
            vipname.style.border = '1px solid #e1e1e1'
        } else {
            vipname.style.border = '1px solid red';
            errors.style.display = 'block';
            errors.style.color = 'red';
        }
    }
    function passwordcheck() {
        let reg = /(?!^[0-9]+$)(?!^[A-z]+$)(?!^[^A-z0-9]+$)^.{6,16}$/;
        let errors1 = spanerror[1]
        if (reg.test(password.value)) {
            errors1.style.display = 'none'
            password.style.border = '1px solid #e1e1e1'
        } else {
            password.style.border = '1px solid red'
            errors1.style.display = 'block'
            errors1.style.color = 'red'
        }
    }
    function repasswordcheck() {
        let errors2 = spanerror[2]
        if (repassword == password) {
            errors2.style.display = 'none'
            repassword.style.border = '1px solid #e1e1e1'
        } else {
            repassword.style.border = '1px solid red'
            errors2.style.display = 'block'
            errors2.style.color = 'red'
        }
    }
    function phonecheck() {
        let errors5 = spanerror[3]
        let reg4 = /^1(3[0-9]|5[189]|8[6789])[0-9]{8}$/;
        if (reg4.test(phone.value)) {
            phone.style.border='1px solid #e1e1e1'
            errors5.style.display = 'none'
        } else {
            phone.style.border='1px solid red'
            errors5.style.display = 'block'
            errors5.style.color = 'red'
        }
}
}
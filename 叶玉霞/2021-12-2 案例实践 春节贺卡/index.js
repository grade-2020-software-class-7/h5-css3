window.onload = function(){
    let music = document.getElementById("music")
    let audio = document.querySelector("audio")
    let page1 = document.getElementById("page1")
    let page2 = document.getElementById("page2")
    let page3 = document.getElementById("page3")

    // 判断音频是否暂停，如果暂停就移除类play，否则给music增加类play
    if(audio.paused){
        music.classList.remove("play")
    }else{
        music.classList.add("play")
    }

    music.addEventListener('touchstart', function() {
        // 控制音乐播放器，播放
        if (audio.paused) {
            audio.play();
            music.classList.add('play');
            music.style.animationPlayState = 'running';
        } else {
            audio.pause();
            music.style.animationPlayState = 'paused';
        }
    });

    page1.addEventListener('touchstart',function(){
        page1.style.display = "none"
        page2.style.display = "block"
        page3.style.display = "block"

        setTimeout(function(){
            page2.classList.add('fade-out')
            page3.classList.add('fade-in')
        },5000)
    })
}